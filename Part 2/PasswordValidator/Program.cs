﻿
// Password Validator

PasswordValidator validator = new PasswordValidator();

while (true)
{
    Console.Write("Please enter a password: ");
    string input = Console.ReadLine();

    bool isValid = validator.Validate(input);

    if (isValid)
    {
        Console.WriteLine("Valid password.");
    }
    else
    {
        Console.WriteLine("Invalid password.");
    }
}
internal class PasswordValidator
{
    public bool Validate(string input)
    {
        if (input.Length < 6 || input.Length > 13) //6-13 character passwords only.
        {
            return false;
        }

        int numOfCaps = 0;
        int numOfLower = 0;
        int numOfNums = 0;
        int illegalChars = 0; // Fixes bug where else if couldn't detect the letter T

        foreach (char letter in input)
        {
            if (char.IsUpper(letter))
            {
                numOfCaps++;
            }
            else if (char.IsLower(letter))
            {
                numOfLower++;
            }
            else if (char.IsNumber(letter))
            {
                numOfNums++;
            }
            else if (letter == 'T' || letter == '&') // No T or &
            {
                illegalChars++;
            }
        }

        if (numOfCaps < 1 || numOfLower < 1 || numOfNums < 1 || illegalChars > 0)
        {
            return false;
        }


        return true; // If all conditions are met
    }
}