﻿
// Room Coordinates

Coordinate a = new Coordinate(2, 3);
Coordinate b = new Coordinate(2, 4);
Coordinate c = new Coordinate(3, 5);

Console.WriteLine($"Coordinate a and b adjacent: {a.IsAdjacent(a, b)}");
Console.WriteLine($"Coordinate c and b adjacent: {a.IsAdjacent(c, b)}");
Console.WriteLine($"Coordinate a and c adjacent: {a.IsAdjacent(a, c)}");

public struct Coordinate
{
    public int _x { get; init; }
    public int _y { get; init; }

    public Coordinate (int x, int y)
    {
        _x = x; 
        _y = y;
    }

    public bool IsAdjacent(Coordinate a, Coordinate b)
    {
        if (a._x - b._x == 1 || a._x - b._x == -1)
        {
            if (a._y == b._y)
            {
                return true;
            }

            return false;
        }

        if (a._y - b._y == 1 || a._y - b._y == -1) 
        {
            if (a._x == b._x)
            {
                return true;
            }
            return false;
        }

        return false;
    }
}