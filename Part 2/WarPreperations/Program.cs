﻿
// War Preperations

Sword original = new Sword(Material.iron, Gemstone.none, 5, 25);

Sword new1 = original with { material = Material.binarium };

Sword new2 = original with { material = Material.steel };

Console.WriteLine(original);
Console.WriteLine(new1);
Console.WriteLine(new2);


public enum Material { wood, bronze, iron, steel, binarium }
public enum Gemstone { emerald, amber, sapphire, diamond, bitstone, none }
public record Sword(Material material, Gemstone gemstone, int crossguardWidth, int length);

