﻿
// The Locked Door

Console.WriteLine("Welcome, to the locked door. To create a new door, enter a new numeric code: ");
int codeInput = Convert.ToInt32(Console.ReadLine());

Door door = new Door(codeInput);

Console.Clear();
Console.WriteLine("Created new door. It is locked.");


while (true)
{
    Console.WriteLine("What would you like to do?\n1.Unlock\n2.Lock\n3.Open\n4.Change passcode");
    int input = Convert.ToInt32(Console.ReadLine());

    switch (input)
    {
        case 1:
            Console.WriteLine("Please enter passcode: ");
            codeInput = Convert.ToInt32(Console.ReadLine());
            door.UnlockDoor(codeInput);
            break;
        case 2:
            door.LockDoor();
            break;
        case 3:
            door.OpenDoor();
            break;
        case 4:
            Console.WriteLine("Please enter current passcode: ");
            codeInput = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Please enter new numeric passcode: ");
            int newCodeInput = Convert.ToInt32(Console.ReadLine());
            door.ChangePass(codeInput, newCodeInput);
            break;
        default:
            Console.WriteLine("Invalid selection.");
            break;
    }
}

internal class Door
{
    private int passcode;
    private DoorState doorState;

    public Door(int code)
    {
        passcode = code;
        doorState = DoorState.Locked;
    }

    public void CloseDoor()
    {
        Console.Clear();
        if (doorState == DoorState.Open)
        {
            doorState = DoorState.Unlocked;
            Console.WriteLine("The door has been closed. It is unlocked.");
        }
        else
        {
            Console.WriteLine($"The door couldn't be closed. It is already closed.");
        }
    }

    public void OpenDoor()
    {
        Console.Clear();
        if (doorState == DoorState.Unlocked)
        {
            doorState = DoorState.Open;
            Console.WriteLine("The door has been opened.");
        }
        else if (doorState == DoorState.Open)
        {
            Console.WriteLine("The door is already open.");
        }
        else
        {
            Console.WriteLine("The door couldn't be opened. It is locked.");
        }
    }

    public void LockDoor()
    {
        Console.Clear();
        if (doorState == DoorState.Unlocked)
        {
            doorState = DoorState.Locked;
            Console.WriteLine("The door has been locked.");
        }
        else if (doorState == DoorState.Locked)
        {
            Console.WriteLine("The door is already locked.");
        }
        else
        {
            Console.WriteLine("The door can't be locked when it is open.");
        }
    }

    public void UnlockDoor(int code)
    {
        Console.Clear();
        if (code == passcode && doorState == DoorState.Locked)
        {
            doorState = DoorState.Unlocked;
            Console.WriteLine("Door has been unlocked.");
        }
        else
        {
            Console.WriteLine("Door could not be unlocked.");
        }
    }

    public void ChangePass(int code, int newcode)
    {
        Console.Clear();
        if (code == passcode)
        {
            Console.WriteLine("Code has been changed.");
            passcode = newcode;
        }
        else
        {
            Console.WriteLine("Incorrect code.");
        }
    }

}

enum DoorState { Open, Unlocked, Locked };