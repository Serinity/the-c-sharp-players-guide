﻿
// Tic-Tac-Toe

Player player1 = new Player('X');
Player player2 = new Player('O');

Game board = new Game();

while (true)
{
    while (true)
    {
        board.DisplayBoard();
        Console.Write("Player 1, please enter a number on the board: ");
        char input = Convert.ToChar(Console.ReadLine());

        if (board.PlaceMark(player1, input))
        {
            break;
        }
        else
        {
            Console.WriteLine("That spot is already taken!");
        }
    }

    while (true)
    {
        board.DisplayBoard();
        Console.Write("Player 2, please enter a number on the board: ");
        char input = Convert.ToChar(Console.ReadLine());

        if (board.PlaceMark(player2, input))
        {
            break;
        }
        else
        {
            Console.WriteLine("That spot is already taken!");
        }
    }
    
}

internal class Player
{
    public char mark;

    public Player(char input)
    {
        mark = input;
    }
}

internal class Game
{
    public int rounds = 0;
    private char[,] board = { { '7', '8', '9' }, { '4', '5', '6' }, { '1', '2', '3' } };

    public void DisplayBoard()
    {
        Console.WriteLine($" {board[0, 0]} | {board[0, 1]} | {board[0, 2]}\n" +
                          $"---+---+---\n" +
                          $" {board[1, 0]} | {board[1, 1]} | {board[1, 2]}\n" +
                          $"---+---+---\n" +
                          $" {board[2, 0]} | {board[2, 1]} | {board[2, 2]}");
    }

    public bool DetectWin(Player player)
    {
        for (int rows = 0; rows < board.GetLength(0); rows++) // Check rows for win
        {
            int inARow = 0;
            for (int cols = 0; cols < board.GetLength(1); cols++)
            {
                if (board[rows, cols] == player.mark)
                {
                    inARow++;
                }
                if (inARow == 3)
                {
                    return true;
                }
            }
        }
        
        for (int cols = 0; cols < board.GetLength(1); cols++) //Check columns for win
        {
            int inACol = 0;
            for (int rows = 0; rows < board.GetLength(0); rows++)
            {
                if (board[rows, cols] == player.mark)
                {
                    inACol++;
                }
                if (inACol == 3)
                {
                    return true;
                }
            }
        }

        if (board[0, 0] == player.mark && board[1, 1] == player.mark && board[2, 2] == player.mark || board[2, 0] == player.mark && board[1, 1] == player.mark && board[0, 2] == player.mark) // Check for diagonal win
        {
            return true;
        }

        return false;
    }

    public bool PlaceMark(Player player, char choice)
    {
        for (int rows = 0; rows < board.GetLength(0); rows++)
        {
            for (int cols = 0; cols < board.GetLength(1); cols++)
            {
                if (board[rows, cols] == choice)
                {
                    board[rows, cols] = player.mark;
                    rounds++;
                    bool HasWon = DetectWin(player);
                    if (HasWon)
                    {
                        Console.Clear();
                        Console.WriteLine($"Player {player.mark} has won in {rounds} rounds!");
                        DisplayBoard();
                        Console.ReadKey(true);
                        System.Environment.Exit(1);
                    }
                    if (rounds >= 9)
                    {
                        Console.Clear();
                        Console.WriteLine("Stalemate! Nobody wins!");
                        DisplayBoard();
                        Console.ReadKey(true);
                        System.Environment.Exit(1);
                    }
                    return true;
                }
            }
        }
        return false;
    }
}