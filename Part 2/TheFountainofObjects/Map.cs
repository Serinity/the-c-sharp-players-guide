﻿using FountainEnums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Map
{
    public class Map
    {
        private RoomType[,] _room;

        public int Rows { get; }
        public int Columns { get; }

        public int PlayerRow { get; set; }
        public int PlayerCol { get; set; }

        public bool FountainEnabled = false;

        public Map(int rows, int columns, int fountainRow, int fountainCol) // Constructor for Map class
        {
            Rows = rows;
            Columns = columns;
            _room = new RoomType[Rows, Columns];
            PlayerRow = 0; // Sets player default position to 0, 0.
            PlayerCol = 0;

            for (int r = 0; r < Rows; r++) // Set all rooms to normal type
            {
                for (int c = 0; c < Columns; c++)
                {
                    _room[r, c] = RoomType.Normal;
                }
            }

            _room[fountainRow, fountainCol] = RoomType.Fountain; // Sets where fountain is
            _room[0, 0] = RoomType.Entrance; // Sets entrance to 0, 0 (where player entered)
        }

        public void MovePlayer(int row, int col)
        {
            if (PlayerRow + row < 0 || PlayerRow + row >= Rows) // Checks that player isn't trying to leave the map
            {
                Console.WriteLine("Can not move off of the map.");
                return;
            }
            else
            {
                PlayerRow += row;
            }
            if (PlayerCol + col < 0 || PlayerCol + col >= Columns)
            {
                Console.WriteLine("Can not move off of the map.");
                return;
            }
            else
            {
                PlayerCol += col;
            }
        }

        public RoomType[,] GetMap()
        {
            return _room;
        }
    }
}