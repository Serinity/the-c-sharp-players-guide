﻿
// The Fountain of Objects

using System;
using Game;
using Map;
using FountainEnums;
using TheFountainofObjects;

namespace TheFountainofObjects
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Game.Game.Run(); // Runs the game
        }

    }
}


