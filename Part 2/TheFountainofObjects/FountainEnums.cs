﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FountainEnums
{
    public enum RoomType { Normal, Entrance, Fountain }
    public enum MapSize { none, small, medium, large }
}
