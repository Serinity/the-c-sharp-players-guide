﻿using FountainEnums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game
{
    public static class Game // Game class. Responsibilities include tracking won state, map size, gameplay loop.
    {
        private static bool hasWon = false;
        private static MapSize mapSize = MapSize.none;

        public static void Run()
        {
            Console.WriteLine("Would you like to play a small, medium, or large map?");
            string? choice = Console.ReadLine();
            Map.Map map;

            while (mapSize == MapSize.none) // Map size selection
            {
                switch (choice)
                {
                    case "small":
                        mapSize = MapSize.small;
                        break;
                    case "medium":
                        mapSize = MapSize.medium;
                        break;
                    case "large":
                        mapSize = MapSize.large;
                        break;
                    default:
                        Console.WriteLine("Please enter a valid command. small, medium, or large.");
                        continue;

                }
            }

            if (mapSize == MapSize.small)
            {
                map = new Map.Map(4, 4, 0, 2); // Rows, Cols, FountainRow, FountainCol
            }
            else if (mapSize == MapSize.medium)
            {
                map = new Map.Map(6, 6, 0, 4);
            }
            else if (mapSize == MapSize.large)
            {
                map = new Map.Map(8, 8, 0, 6);
            }
            else
            {
                map = new Map.Map(0, 0, 0, 0); // This should NEVER happen... For some reason this is necessary to make the compiler happy. All praise the compiler.
            }

            Console.Clear(); // Wipe the board for the main gameplay loop

            DateTime startTime = DateTime.Now;

            while (!hasWon)
            {
                Console.ForegroundColor = ConsoleColor.DarkCyan;
                Console.WriteLine("--------------------------------------------------");
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine($"You are in the room at (Row={map.PlayerRow} Column={map.PlayerCol}).");

                RoomType[,] PlayingMap = map.GetMap(); // Gets the map in the form of a 2D array of RoomTypes

                if (PlayingMap[map.PlayerRow, map.PlayerCol] == RoomType.Entrance) // If entrance
                {
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine("You see light coming from the cavern entrance.");
                }
                else if (PlayingMap[map.PlayerRow, map.PlayerCol] == RoomType.Fountain) // If fountain
                {
                    Console.ForegroundColor = ConsoleColor.Blue;
                    if (map.FountainEnabled)
                    {
                        Console.WriteLine("You hear the rushing waters from the Fountain of Objects. It has been reactivated!");
                    }
                    else
                    {
                        Console.WriteLine("You hear water dripping in this room. The Fountain of Objects is here!");
                    }
                }
                Console.ForegroundColor = ConsoleColor.White;
                Console.Write("What would you like to do? ");
                string? input = Console.ReadLine();

                switch (input) // Main user input
                {
                    case "move north":
                        map.MovePlayer(-1, 0);
                        break;
                    case "move south":
                        map.MovePlayer(1, 0);
                        break;
                    case "move east":
                        map.MovePlayer(0, 1);
                        break;
                    case "move west":
                        map.MovePlayer(0, -1);
                        break;
                    case "enable fountain":
                        if (PlayingMap[map.PlayerRow, map.PlayerCol] == RoomType.Fountain) // Only allow fountain to be enabled if player is in the room with the fountain
                        {
                            map.FountainEnabled = true;
                        }
                        else
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("The fountain is not in this room.");
                        }
                        break;
                    case "disable fountain":
                        if (PlayingMap[map.PlayerRow, map.PlayerCol] == RoomType.Fountain)
                        {
                            map.FountainEnabled = false;
                        }
                        else
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("The fountain is not in this room.");
                        }
                        break;
                    case "help":
                        Console.ForegroundColor = ConsoleColor.Blue;
                        Console.WriteLine("Enter one of the following commands: move north - Moves 1 room up\nmove south - Moves 1 room down\nmove east - Moves 1 room right\nmove west - Moves 1 room left\nenable fountain - Activates the Fountain of Objects\ndisable fountain - Deactivates the Fountain of Objects\nhelp - You're already here, silly!");
                        break;
                    default:
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("Please enter a valid command.");
                        break;
                }

                if (map.PlayerRow == 0 && map.PlayerCol == 0 && map.FountainEnabled) // Checks for win state
                {
                    DateTime endTime = DateTime.Now;
                    TimeSpan timeSpent = endTime - startTime;

                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine($"The Fountain of Objects has been reactivated, and you have escaped with your life in {timeSpent.Minutes} minutes and {timeSpent.Seconds} seconds!\nYou win!");
                    Console.ForegroundColor = ConsoleColor.White;
                    hasWon = true;
                    break;
                }

            }
        }
    }
}