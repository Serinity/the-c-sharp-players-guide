﻿
// Vin Fletcher's Arrows

Console.WriteLine("Welcome to Vin's Arrow Shop. What would you like today?\nElite Arrow\nBeginner Arrow\nMarksman Arrow\nCustom Arrow");

string input = Console.ReadLine();

Arrow arrow;

if (input == "Elite Arrow")
{
    arrow = Arrow.CreateEliteArrow();
}
else if (input == "Beginner Arrow")
{
    arrow = Arrow.CreateBeginnerArrow();
}
else if (input == "Marksman Arrow")
{
    arrow = Arrow.CreateMarksmanArrow();
}
else
{
    arrow = new Arrow() { arrowhead = GetArrowHead(), fletching = GetFletchingType(), length = GetArrowLength() }; // Make custom arrow
}


Console.WriteLine($"Your new arrow with a {arrow.arrowhead} head, {arrow.fletching} fletching, and length of {arrow.length} costs {arrow.GetCost(arrow.arrowhead, arrow.fletching, arrow.length)} monopoly monies.");

Arrowhead GetArrowHead()
{
    Console.WriteLine("Arrowhead type: Steel, Wood, Obsidian");
    string input = Console.ReadLine();
    return input switch
    {
        "Steel" => Arrowhead.Steel,
        "Wool" => Arrowhead.Wood,
        "Obsidian" => Arrowhead.Obsidian
    };
}

Fletching GetFletchingType()
{
    Console.WriteLine("Fletching type: Plastic, TurkeyFeathers, GooseFeathers");
    string input = Console.ReadLine();
    return input switch
    {
        "Plastic" => Fletching.Plastic,
        "TurkeyFeathers" => Fletching.TurkeyFeathers,
        "GooseFeathers" => Fletching.GooseFeathers
    };
}

int GetArrowLength()
{
    Console.Write("Arrowhead length between 60 and 100: ");
    int input = Convert.ToInt32(Console.ReadLine());
    return input;
}

internal class Arrow
{
    public float length { get; init; }
    public Arrowhead arrowhead { get; set; }
    public Fletching fletching { get; set; }

    public float GetCost(Arrowhead ah, Fletching fl, float len)
    {
        float arrowheadCost = ah switch
        {
            Arrowhead.Steel => 10.0f,
            Arrowhead.Obsidian => 5.0f,
            Arrowhead.Wood => 3.0f
        };

        float fletchingCost = fl switch
        {
            Fletching.Plastic => 10.0f,
            Fletching.TurkeyFeathers => 5.0f,
            Fletching.GooseFeathers => 3.0f
        };

        return arrowheadCost + fletchingCost + (len * 0.05f);
    }

    public static Arrow CreateEliteArrow()
    {
        Arrow elite = new Arrow() { arrowhead = Arrowhead.Steel, fletching = Fletching.Plastic, length = 95.0f };

        return elite;
    }

    public static Arrow CreateBeginnerArrow()
    {
        Arrow beginner = new Arrow() { arrowhead = Arrowhead.Wood, fletching = Fletching.GooseFeathers, length = 75.0f };

        return beginner;
    }

    public static Arrow CreateMarksmanArrow()
    {
        Arrow marksman = new Arrow() { arrowhead = Arrowhead.Steel, fletching = Fletching.GooseFeathers, length = 65.0f };

        return marksman;
    }

}

enum Arrowhead { Steel, Wood, Obsidian }
enum Fletching { Plastic, TurkeyFeathers, GooseFeathers }