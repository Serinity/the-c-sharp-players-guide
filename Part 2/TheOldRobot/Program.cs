﻿
// The Old Robot and Robotic Interface

Robot robot = new Robot();

while (true)
{
    bool cont = true;
    while (cont)
    {
        string? input = Console.ReadLine();

        IRobotCommand robotCommand;

        switch (input)
        {
            case "on":
                robotCommand = new OnCommand();
                robot.Commands.Add(robotCommand);
                continue;
            case "off":
                robotCommand = new OffCommand();
                robot.Commands.Add(robotCommand);
                continue;
            case "north":
                robotCommand = new NorthCommand();
                robot.Commands.Add(robotCommand);
                continue;
            case "south":
                robotCommand = new SouthCommand();
                robot.Commands.Add(robotCommand);
                continue;
            case "east":
                robotCommand = new EastCommand();
                robot.Commands.Add(robotCommand);
                continue;
            case "west":
                robotCommand = new WestCommand();
                robot.Commands.Add(robotCommand);
                continue;
            case "stop":
                cont = false;
                break;
            default:
                Console.WriteLine("Invalid command. Shutting down robot..."); // It's not a bug, it's a feature!
                robotCommand = new OffCommand(); // Idk why I need this to make it work, but it works.
                continue;
        }


    }
    Console.WriteLine("");
    robot.Run();
    Console.WriteLine("");
}

public class Robot
{
    public int X { get; set; }
    public int Y { get; set; }
    public bool IsPowered { get; set; }
    public List<IRobotCommand?> Commands = new List<IRobotCommand?>();
    public void Run()
    {
        foreach (IRobotCommand? command in Commands)
        {
            command?.Run(this);
            Console.WriteLine($"[{X} {Y} {IsPowered}]");
        }
    }
}

public interface IRobotCommand
{
    public abstract void Run(Robot robot);
}

internal class OnCommand : IRobotCommand
{
    public void Run(Robot robot)
    {
        robot.IsPowered = true;
    }
}

internal class OffCommand : IRobotCommand
{
    public void Run(Robot robot)
    {
        robot.IsPowered = false;
    }
}

internal class NorthCommand : IRobotCommand
{
    public void Run(Robot robot)
    {
        if (robot.IsPowered)
        {
            robot.Y++;
        }
    }
}

internal class SouthCommand : IRobotCommand
{
    public void Run(Robot robot)
    {
        if (robot.IsPowered)
        {
            robot.Y--;
        }
    }
}

internal class EastCommand : IRobotCommand
{
    public void Run(Robot robot)
    {
        if (robot.IsPowered)
        {
            robot.X++;
        }
    }
}

internal class WestCommand : IRobotCommand
{
    public void Run(Robot robot)
    {
        if (robot.IsPowered)
        {
            robot.X--;
        }
    }
}