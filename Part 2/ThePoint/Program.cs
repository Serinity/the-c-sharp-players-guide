﻿
// The Point

Point point1 = new Point(2, 3);
Point point2 = new Point(-4, 0);

Console.WriteLine($"({point1.GetX()}, {point1.GetY()})\n({point2.GetX()}, {point2.GetY()})");

internal class Point
{
    private int _x;
    private int _y;

    public Point(int x, int y)
    {
        _x = x;
        _y = y;
    }

    public Point()
    {
        _x = 0;
        _y = 0;
    }

    public int GetX()
    {
        return _x;
    }

    public int GetY()
    {
        return _y;
    }
}