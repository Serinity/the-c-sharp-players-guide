﻿
// Simula's Test

Chest chest = Chest.Locked;

while (true)
{
    if (chest == Chest.Locked)
    {
        Console.Write("The chest is locked. What do you want to do?: ");
        string input = Console.ReadLine();

        if (input == "unlock")
        {
            chest = Chest.Unlocked;
            continue;
        }
        else
        {
            continue;
        }
    }
    else if (chest == Chest.Unlocked)
    {
        Console.Write("The chest is unlocked. What do you want to do?: ");
        string input = Console.ReadLine();

        if (input == "open")
        {
            chest = Chest.Open;
            continue;
        }
        else if (input == "lock")
        {
            chest = Chest.Locked;
            continue;
        }
        else
        {
            continue;
        }
    }
    else // Chest open
    {
        Console.Write("The chest is open. What do you want to do?: ");
        string input = Console.ReadLine();

        if (input == "close")
        {
            chest = Chest.Unlocked;
            continue;
        }
        else
        {
            continue;
        }
    }
}

enum Chest { Open, Unlocked, Locked }; // Defining chest enumeration type