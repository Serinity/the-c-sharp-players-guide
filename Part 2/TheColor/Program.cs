﻿
// The Color

using System.ComponentModel;

Color color1 = new Color(255, 1, 56);
Color color2 = Color.Black();

Console.WriteLine($"R: {color1.GetR()} G: {color1.GetG()} B: {color1.GetB()}\nR: {color2.GetR()} G: {color2.GetG()} B: {color2.GetB()}");

internal class Color
{
    private int R;
    private int G;
    private int B;

    public Color(int r, int g, int b)
    {
        this.R = r;
        this.G = g;
        this.B = b;
    }

    public int GetR()
    {
        return this.R;
    }

    public int GetG()
    {
        return this.G;
    }

    public int GetB()
    {
        return this.B;
    }

    public static Color Red()
    {
        return new Color(255, 0, 0);
    }

    public static Color Green()
    {
        return new Color(0, 128, 0);
    }

    public static Color Blue()
    {
        return new Color(0, 0, 255);
    }

    public static Color White()
    {
        return new Color(255, 255, 255);
    }

    public static Color Black()
    {
        return new Color(0, 0, 0);
    }

    public static Color Orange()
    {
        return new Color(255, 165, 0);
    }

    public static Color Yellow()
    {
        return new Color(255, 255, 0);
    }

    public static Color Purple()
    {
        return new Color(128, 0, 128);
    }
}