﻿
// Simula's Soup

(Type Type, Main Main, Seasoning Seasoning) Soup = (Type.Soup, Main.Mushrooms, Seasoning.Spicy); // Order matters


Console.WriteLine("Welcome to Simula's Soups! Please select one of the following types.");
Console.WriteLine("Soup\nStew\nGumbo");
string input = Console.ReadLine();

if (input == "Soup" || input == "soup")
{
    Soup.Type = Type.Soup;
}
else if (input == "Stew" || input == "stew")
{
    Soup.Type = Type.Stew;
}
else if (input == "Gumbo" || input == "gumbo")
{
    Soup.Type = Type.Gumbo;
}
else
{
    Console.WriteLine("Invalid input.");
}

Console.WriteLine("Great! Now choose one of the following mains:\nMushrooms\nChicken\nCarrots\nPotatoes");
input = Console.ReadLine();

if (input == "Mushrooms" || input == "mushrooms")
{
    Soup.Main = Main.Mushrooms;
}
else if (input == "Chicken" || input == "chicken")
{
    Soup.Main = Main.Chicken;
}
else if (input == "Carrots" || input == "carrots")
{
    Soup.Main = Main.Carrots;
}
else if (input == "Potatoes" || input == "potatoes")
{
    Soup.Main = Main.Potatoes;
}
else
{
    Console.WriteLine("Invalid selection.");
}

Console.WriteLine("Awesome. Now choose a seasoning:\nSpicy\nSalty\nSweet");
input = Console.ReadLine();

if (input == "Spicy" || input == "spicy")
{
    Soup.Seasoning = Seasoning.Spicy;
}
else if (input == "Salty" || input == "salty")
{
    Soup.Seasoning = Seasoning.Salty;
}
else if (input == "Sweet" || input == "sweet")
{
    Soup.Seasoning = Seasoning.Sweet;
}
else
{
    Console.WriteLine("Invalid selection.");
}

Console.WriteLine($"Congrats! You've made a {Soup.Seasoning} {Soup.Main} {Soup.Type}.");

enum Type { Soup, Stew, Gumbo };
enum Main { Mushrooms, Chicken, Carrots, Potatoes };
enum Seasoning { Spicy, Salty, Sweet };