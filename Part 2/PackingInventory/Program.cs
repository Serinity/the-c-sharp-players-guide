﻿
// Packing Inventory & Labeling Inventory

Pack pack = new Pack() { totalMax = 4, weightMax = 15.0f, volumeMax = 10.0f };

while (true) // Gameplay loop
{
    Console.Clear();
    Console.WriteLine(pack.ToString());
    Console.WriteLine($"Currently, your backpack can hold {pack.totalMax - pack.CurrentTotal()} more items. \nPack weight: {pack.CurrentWeight()}/{pack.weightMax}\nPack volume: {pack.CurrentVolume()}/{pack.volumeMax}");
    Console.WriteLine("What would you like to add?\n1.Arrow\n2.Bow\n3.Rope\n4.Water\n5.Food Rations\n6.Sword");
    int input = Convert.ToInt32(Console.ReadLine());

    bool canFit;

    switch (input)
    {
        case 1:
            Arrow arrow = new Arrow();
            canFit = pack.Add(arrow);
            break;
        case 2:
            Bow bow = new Bow();
            canFit = pack.Add(bow);
            break;
        case 3:
            Rope rope = new Rope();
            canFit = pack.Add(rope);
            break;
        case 4:
            Water water = new Water();
            canFit = pack.Add(water);
            break;
        case 5:
            Food food = new Food();
            canFit = pack.Add(food);
            break;
        case 6:
            Sword sword = new Sword();
            canFit = pack.Add(sword);
            break;
        default:
            Console.WriteLine("Invalid selection. Press any key to continue.");
            Console.ReadKey(true);
            canFit = true; // So it doesn't warn the user null item can't fit
            break;
    }

    if (!canFit)
    {
        Console.WriteLine("That item can't fit in your bag! Press any key to try again.");
        Console.ReadKey(true);
    }

}

internal class Pack
{
    public int totalMax { get; init; }
    public float weightMax { get; init; }
    public float volumeMax { get; init; }

    private int currentTotal = 0;
    private float currentWeight = 0.0f;
    private float currentVolume = 0.0f;

    public override string ToString()
    {
        string contents= "";

        foreach (InventoryItem item in inventoryItems)
        {
            contents += " " + item;
        }

        if (contents == "")
        {
            return "";
        }
        else
        {
            return "Pack containing" + contents;
        }
        
    }

    private InventoryItem[] inventoryItems {  get; set; } = new InventoryItem[0];

    public bool Add(InventoryItem item)
    {
        if (currentTotal < totalMax && item._weight + currentWeight < weightMax && item._volume + currentVolume < volumeMax)
        {
            inventoryItems = inventoryItems.Append(item).ToArray();
            currentTotal++;
            currentWeight = currentWeight + item._weight;
            currentVolume = currentVolume + item._volume;
            return true;
        }
        
        return false;
    }

    public int CurrentTotal()
    {
        return currentTotal;
    }

    public float CurrentWeight()
    {
        return currentWeight;
    }

    public float CurrentVolume()
    {
        return currentVolume;
    }
}

internal class InventoryItem
{
    public float _weight;
    public float _volume;

    public InventoryItem(float weight, float volume)
    {
        _weight = weight;
        _volume = volume;
    }
}

internal class Arrow : InventoryItem
{
    public Arrow() : base(0.1f, 0.05f) { }

    public override string ToString()
    {
        return "Arrow";
    }
}

internal class Bow : InventoryItem
{
    public Bow() : base(1.0f, 4.0f) { }

    public override string ToString()
    {
        return "Bow";
    }
}

internal class Rope : InventoryItem
{
    public Rope() : base(1.0f, 1.5f) { }

    public override string ToString()
    {
        return "Rope";
    }
}

internal class Water : InventoryItem
{
    public Water() : base(2.0f, 3.0f) { }

    public override string ToString()
    {
        return "Water";
    }
}

internal class Food : InventoryItem
{
    public Food() : base(1.0f, 0.5f) { }

    public override string ToString()
    {
        return "Food";
    }
}

internal class Sword : InventoryItem
{
    public Sword() : base(5.0f, 3.0f) { }

    public override string ToString()
    {
        return "Sword";
    }
}