﻿
// The Card

Color[] colors = new Color[] { Color.red, Color.green, Color.blue, Color.yellow };
Rank[] ranks = new Rank[] { Rank.one, Rank.two, Rank.three, Rank.four, Rank.five, Rank.six, Rank.seven, Rank.eight, Rank.nine, Rank.ten, Rank.usd, Rank.percent, Rank.carrot, Rank.and };

foreach (Color color in colors)
{
    foreach (Rank rank in ranks)
    {
        Card card = new Card(color, rank);
        Console.WriteLine($"The {card.CardColor} {card.CardRank}");
    }
}

internal class Card
{
    public Color CardColor { get; set; }
    public Rank CardRank { get; set; }

    public Card(Color cc, Rank cr)
    {
        this.CardRank = cr;
        this.CardColor = cc;
    }

    public bool IsFaceCard()
    {
        if (CardRank == Rank.usd || CardRank == Rank.percent || CardRank == Rank.carrot || CardRank == Rank.and)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

}
enum Color { red, green, blue, yellow };
enum Rank { one, two, three, four, five, six, seven, eight, nine, ten, usd, percent, carrot, and};