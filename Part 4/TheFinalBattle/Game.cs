﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Spectre.Console;
using Spectre.Console.Cli;

public static class Game
{
    public static List<Character> _heroParty = new List<Character>();
    public static List<Character> _monsterParty = new List<Character>();
    public static List<Item> _heroPartyInventory = new List<Item>();
    public static List<Item> _monsterPartyInventory = new List<Item>();
    public static List<Gear> _heroPartyGear = new List<Gear>();
    public static List<Gear> _monsterPartyGear = new List<Gear>();
    public static GameMode _gameMode;
    public static GameSpeed _gameSpeed;
    public static int _round;
    public static Random random = new Random();
    public static int _heroXP = 0;
    public static int _monsterXP = 0;

    public static void GameDelay()
    {
        switch (_gameSpeed)
        {
            case GameSpeed.Slow:
                Thread.Sleep(6000);
                break;
            case GameSpeed.Medium:
                Thread.Sleep(4000);
                break;
            case GameSpeed.Fast:
                Thread.Sleep(3000);
                break;
            case GameSpeed.Manual:
                Console.WriteLine();
                Console.WriteLine("Press any key to continue");
                Console.ReadKey(true);
                break;
        }
    }

    public static void WinState()
    {
        Console.Clear();
        if (_monsterParty.Count == 0) Console.WriteLine("\n\n\n\n\n\n\n\n\n\n\n\n\n\n                                  The heroes have won, and The Uncoded One was defeated!\n\n\n");
        if (_heroParty.Count == 0) Console.WriteLine("\n\n\n\n\n\n\n\n\n\n\n\n\n\n                            The heroes have lost, and The Uncoded One's forces have prevailed...\n\n\n");
        AnsiConsole.Write(new BarChart()
            .Width(50)
            .AddItem("     Hero Party XP", _heroXP, Color.Yellow)
            .AddItem("     Monster Party XP", _monsterXP, Color.Red));
        Console.ReadKey(true);
        Environment.Exit(0);
    }

    public static void HeroPartyTurn()
    {
        List<Character> queue = new List<Character>();

        foreach (Character character in _heroParty)
        {
            queue.Add(character);
        }

        do
        {
            foreach (Character character in queue)
            {
                if (_monsterParty.Count == 0) break;
                AnsiConsole.Write(new Markup($"It is [steelblue1_1]{character._name}[/]'s turn...\n"));
                if (character._tempEffect == TempEffect.Piercing && character._tempEffectRounds < 3)
                {
                    AnsiConsole.Write(new Markup($"[steelblue1_1]{character._name}[/] is bleeding from piercing damage!\n"));
                    character.Defend(1, character, false);
                    character._tempEffectRounds += 1;
                    Game.GameDelay();
                    if (character._health == 0)
                    {
                        queue.Remove(character);
                        break;
                    }
                }
                else if (character._tempEffectRounds == 3)
                {
                    character._tempEffectRounds = 0;
                    character._tempEffect = TempEffect.None;
                }
                if (_gameMode == GameMode.ComputerVComputer && _monsterParty.Count > 0) character.Action(ComputerController.GetRandomAction(character), _monsterParty[ComputerController.GetRandom(_monsterParty.Count - 1)]);
                else character.Action(character.GetChoice(), _monsterParty[ComputerController.GetRandom(_monsterParty.Count - 1)]);
                if (character._health == 0 && _heroParty.Count == 0) break;
                if (character._health == 0)
                {
                    queue.Remove(character);
                    break;
                }
                else
                {
                    queue.Remove(character);
                    break;
                }

            }
            if (_monsterParty.Count == 0) break;
            else if (queue.Count > 0) continue;
            else break;
        } while (true);

    }

    public static void MonsterPartyTurn()
    {
        List<Character> queue = new List<Character>();

        foreach (Character character in _monsterParty)
        {
            queue.Add(character);
        }

        do
        {
            foreach (Character character in queue)
            {
                if (_heroParty.Count == 0) break;
                AnsiConsole.Write(new Markup($"It is [steelblue1_1]{character._name}[/]'s turn...\n"));
                if (character._tempEffect == TempEffect.Piercing && character._tempEffectRounds < 3)
                {
                    AnsiConsole.Write(new Markup($"[steelblue1_1]{character._name}[/] is bleeding from piercing damage!\n"));
                    character.Defend(1, character, true);
                    character._tempEffectRounds += 1;
                    Game.GameDelay();
                    if (character._health == 0)
                    {
                        queue.Remove(character);
                        break;
                    }
                }
                else if (character._tempEffectRounds == 3)
                {
                    character._tempEffectRounds = 0;
                    character._tempEffect = TempEffect.None;
                }
                if (_gameMode == GameMode.ComputerVComputer || _gameMode == GameMode.PlayerVComputer && _heroParty.Count > 0) character.Action(ComputerController.GetRandomAction(character), _heroParty[ComputerController.GetRandom(_heroParty.Count - 1)]);
                else character.Action(character.GetChoice(), _heroParty[ComputerController.GetRandom(_heroParty.Count - 1)]);
                if (character._health == 0 && _monsterParty.Count == 0) break;
                if (character._health == 0)
                {
                    queue.Remove(character);
                    break;
                }
                else
                {
                    queue.Remove(character);
                    break;
                }
                
            }
            if (_heroParty.Count == 0) break;
            else if (queue.Count > 0) continue;
            else break;
        } while (true);
    }

    public static void PrintGame(bool heroTurn)
    {
        Table table = new Table();
        Console.Clear();
        Console.ForegroundColor = ConsoleColor.Cyan;
        Console.Write("=====================================================[{ ");
        Console.ForegroundColor = ConsoleColor.Yellow;
        Console.Write($"Level {_round}");
        Console.ForegroundColor = ConsoleColor.Cyan;
        Console.Write(" }]=====================================================\n");

        table.Expand();
        table.Border(TableBorder.SimpleHeavy);
        table.AddColumn("[green3]HEROES[/]");
        table.AddColumn("[maroon]MONSTERS[/]");
        table.Columns[1].RightAligned();
        int minCharactersInParty;
        Console.ForegroundColor = ConsoleColor.White;

        string equippedItemHero;
        string equippedItemMonster;
        if (_monsterParty.Count < _heroParty.Count)
        {
            minCharactersInParty = _monsterParty.Count;
            for (int i = 0; i < _monsterParty.Count; i++)
            {
                equippedItemHero = GetItemHero(i);
                equippedItemMonster = GetItemMonster(i);

                if (heroTurn) table.AddRow($"[steelblue1_1]{_heroParty[i]._name}[/]{equippedItemHero}: [red]{_heroParty[i]._health}/{_heroParty[i]._maxHealth} HP[/]", $"{_monsterParty[i]._name}{equippedItemMonster}: [red]{_monsterParty[i]._health}/{_monsterParty[i]._maxHealth} HP[/]");
                else table.AddRow($"{_heroParty[i]._name}{equippedItemHero}: [red]{_heroParty[i]._health}/{_heroParty[i]._maxHealth} HP[/]", $"[steelblue1_1]{_monsterParty[i]._name}[/]{equippedItemMonster}: [red]{_monsterParty[i]._health}/{_monsterParty[i]._maxHealth} HP[/]");
            }
            for (int i = minCharactersInParty; i < _heroParty.Count; i++)
            {
                equippedItemHero = GetItemHero(i);

                if (heroTurn) table.AddRow($"[steelblue1_1]{_heroParty[i]._name}[/]{equippedItemHero}: [red]{_heroParty[i]._health}/{_heroParty[i]._maxHealth} HP[/]", "");
                else table.AddRow($"{_heroParty[i]._name}{equippedItemHero}: [red]{_heroParty[i]._health}/{_heroParty[i]._maxHealth} HP[/]", "");
            }
        }
        else
        {
            minCharactersInParty = _heroParty.Count();

            for (int i = 0; i < _heroParty.Count; i++)
            {
                equippedItemHero = GetItemHero(i);
                equippedItemMonster = GetItemMonster(i);

                if (heroTurn) table.AddRow($"[steelblue1_1]{_heroParty[i]._name}[/]{equippedItemHero}: [red]{_heroParty[i]._health}/{_heroParty[i]._maxHealth} HP[/]", $"{_monsterParty[i]._name}{equippedItemMonster}: [red]{_monsterParty[i]._health}/{_monsterParty[i]._maxHealth} HP[/]");
                else table.AddRow($"{_heroParty[i]._name}{equippedItemHero}: [red]{_heroParty[i]._health}/{_heroParty[i]._maxHealth} HP[/]", $"[steelblue1_1]{_monsterParty[i]._name}[/]{equippedItemMonster}: [red]{_monsterParty[i]._health}/{_monsterParty[i]._maxHealth} HP[/]");
            }
            for (int i = minCharactersInParty; i < _monsterParty.Count; i++)
            {
                equippedItemMonster = GetItemMonster(i);
                if (heroTurn) table.AddRow("", $"{_monsterParty[i]._name}{equippedItemMonster}: [red]{_monsterParty[i]._health}/{_monsterParty[i]._maxHealth} HP[/]");
                else table.AddRow("", $"[steelblue1_1]{_monsterParty[i]._name}[/]{equippedItemMonster}: [red]{_monsterParty[i]._health}/{_monsterParty[i]._maxHealth} HP[/]");
            }
        }

        table.AddRow("-------------------------------", "-------------------------------");
        table.AddRow("", "");
        table.AddRow("Party Inventory:", "Party Inventory:");
        List<string> combinedInventoryHeroes = new List<string>();
        List<string> combinedInventoryMonsters = new List<string>();
        int minItems;

        foreach (Item item in _monsterPartyInventory)
        {
            if (item == Item.HealthPotion) combinedInventoryMonsters.Add("[deeppink4_2]Health Potion[/]");
            else if (item == Item.SimulasSoup) combinedInventoryMonsters.Add("[tan]Simula's Soup[/]");
        }
        foreach (Item item in _heroPartyInventory)
        {
            if (item == Item.HealthPotion) combinedInventoryHeroes.Add("[deeppink4_2]Health Potion[/]");
            else if (item == Item.SimulasSoup) combinedInventoryHeroes.Add("[tan]Simula's Soup[/]");
        }
        foreach (Gear gear in _monsterPartyGear)
        {
            if (gear == Gear.Dagger) combinedInventoryMonsters.Add("[grey46]Dagger[/]");
            else if (gear == Gear.Sword) combinedInventoryMonsters.Add("[lightcyan1]Sword[/]");
            else if (gear == Gear.Bow) combinedInventoryMonsters.Add("[navajowhite1]Vin's Bow[/]");
            else if (gear == Gear.CannonofConsolas) combinedInventoryMonsters.Add("[darkviolet]Cannon of Consolas[/]");
            else if (gear == Gear.BinaryHelm) combinedInventoryMonsters.Add("[slateblue3]Binary Helm[/]");
        }
        foreach (Gear gear in _heroPartyGear)
        {
            if (gear == Gear.Dagger) combinedInventoryHeroes.Add("[grey46]Dagger[/]");
            else if (gear == Gear.Sword) combinedInventoryHeroes.Add("[lightcyan1]Sword[/]");
            else if (gear == Gear.Bow) combinedInventoryHeroes.Add("[navajowhite1]Vin's Bow[/]");
            else if (gear == Gear.CannonofConsolas) combinedInventoryHeroes.Add("[darkviolet]Cannon of Consolas[/]");
            else if (gear == Gear.BinaryHelm) combinedInventoryHeroes.Add("[slateblue3]Binary Helm[/]");
        }

        if (combinedInventoryMonsters.Count < combinedInventoryHeroes.Count)
        {
            minItems = combinedInventoryMonsters.Count;
            for (int i = 0; i < combinedInventoryMonsters.Count; i++)
            {
                table.AddRow($"{combinedInventoryHeroes[i]}", $"{combinedInventoryMonsters[i]}");
            }
            for (int i = minItems; i < combinedInventoryHeroes.Count; i++)
            {
                table.AddRow($"{combinedInventoryHeroes[i]}", "");
            }
        }  
        else
        {
            minItems = combinedInventoryHeroes.Count;
            for (int i = 0; i < combinedInventoryHeroes.Count; i++)
            {
                table.AddRow($"{combinedInventoryHeroes[i]}", $"{combinedInventoryMonsters[i]}");
            }
            for (int i = minItems; i < combinedInventoryMonsters.Count; i++)
            {
                table.AddRow("", $"{combinedInventoryMonsters[i]}");
            }
        }
        table.AddRow("", "");
        table.AddRow("-------------------------------", "-------------------------------");
        AnsiConsole.Write(table);
        AnsiConsole.Write(new BarChart()
            .Width(50)
            .AddItem("Hero Party XP", _heroXP, Color.Yellow)
            .AddItem("Monster Party XP", _monsterXP, Color.Red));
        Console.WriteLine();
        Console.ForegroundColor = ConsoleColor.Cyan;
        Console.WriteLine("=======================================================================================================================\n");
        Console.ForegroundColor = ConsoleColor.White;

        
    }

    private static string GetItemMonster(int i)
    {
        string equippedItemMonster;
        if (_monsterParty[i]._equippedItem == Gear.Sword) equippedItemMonster = " ([lightcyan1]Sword[/])";
        else if (_monsterParty[i]._equippedItem == Gear.Dagger) equippedItemMonster = " ([grey46]Dagger[/])";
        else if (_monsterParty[i]._equippedItem == Gear.Bow) equippedItemMonster = " ([navajowhite1]Vin's Bow[/])";
        else if (_monsterParty[i]._equippedItem == Gear.CannonofConsolas) equippedItemMonster = " ([darkviolet]Cannon of Consolas[/])";
        else if (_monsterParty[i]._equippedItem == Gear.BinaryHelm) equippedItemMonster = " ([slateblue3]Binary Helm[/])";
        else equippedItemMonster = "";
        return equippedItemMonster;
    }

    private static string GetItemHero(int i)
    {
        string equippedItemHero;
        if (_heroParty[i]._equippedItem == Gear.Sword) equippedItemHero = " ([lightcyan1]Sword[/])";
        else if (_heroParty[i]._equippedItem == Gear.Dagger) equippedItemHero = " ([grey46]Dagger[/])";
        else if (_heroParty[i]._equippedItem == Gear.Bow) equippedItemHero = " ([navajowhite1]Vin's Bow[/])";
        else if (_heroParty[i]._equippedItem == Gear.CannonofConsolas) equippedItemHero = " ([darkviolet]Cannon of Consolas[/])";
        else if (_heroParty[i]._equippedItem == Gear.BinaryHelm) equippedItemHero = " ([slateblue3]Binary Helm[/])";
        else equippedItemHero = "";
        return equippedItemHero;
    }

    public static void DisplayLevelScreen()
    {
        Console.Clear();
        Console.Beep();
        Console.CursorVisible = false;
        Console.ForegroundColor = ConsoleColor.Yellow;
        Console.WriteLine($"\n\n\n\n\n\n\n\n\n\n\n\n\n\n                                                        Level {_round}");
        Thread.Sleep(5000);

        Console.CursorVisible = true;
        Console.ForegroundColor = ConsoleColor.White;
    }

    public static void DisplayTutorial()
    {
        int pageNum = 1;
        TutorialHeader(pageNum);

        AnsiConsole.MarkupLine("Welcome, [purple]True Programmer[/]! After your long and harrowing jounrey, you have arrived at the lair of THE UNCODED ONE.\nOnly by defeating his forces will you prevail over him and save the lands of programming, and the city of Consolas.\n");
        AnsiConsole.MarkupLine("Through facing 89+ challenges, you are finally ready to face THE UNCODED ONE. Will you be able to stop his evil doing?");
        Console.WriteLine("\n\nPress any key to continue...");
        Console.ReadKey(true);

        pageNum++;
        TutorialHeader(pageNum);

        Table tutorialTable = new Table();
        tutorialTable.Expand();
        tutorialTable.Border(TableBorder.Rounded);
        tutorialTable.AddColumn("");
        tutorialTable.AddColumn("Controls");
        tutorialTable.AddColumn("");
        tutorialTable.Columns[0].RightAligned();
        tutorialTable.Columns[1].Centered();
        tutorialTable.Columns[2].LeftAligned();
        tutorialTable.AddRow("", "", "");
        tutorialTable.AddRow("Navigation:", "↑\n↓", "Up Arrow Key\nDown Arrow Key");
        tutorialTable.AddRow("", "", "");
        tutorialTable.AddRow("Selection:", "<ENTER>", "ENTER key");
        tutorialTable.AddRow("", "", "");
        tutorialTable.AddRow("Typing:", "Your keyboard, silly.", "");
        tutorialTable.AddRow("", "", "");

        AnsiConsole.Write(tutorialTable);
        Console.WriteLine("\n\nPress any key to continue...");

        Console.ReadKey(true);

        pageNum++;
        Console.Clear();
        TutorialHeader(pageNum);
        Table tutorialTable2 = new Table();
        tutorialTable2.Expand();
        tutorialTable2.Border(TableBorder.Rounded);
        tutorialTable2.Title("HEROES and MONSTERS");
        tutorialTable2.AddColumn("Name");
        tutorialTable2.AddColumn("Description");
        tutorialTable2.AddColumn("Abilities and Items");
        tutorialTable2.AddColumn("Team");
        tutorialTable2.AddRow("[red]True Programmer[/]", "[red]This is you! You name the True Programmer after the tutorial. You've fought long and hard, and are ready to face the Monsters.[/]", "[red]Punch | Sword | Object Sight[/]", "[red]HEROES[/]");
        tutorialTable2.AddRow("[orange3]Vin Fletcher[/]", "[orange3]An arrowmaker who has joined your fight against the Uncoded One.[/]", "[orange3]Punch | Vin's Bow[/]", "[orange3]HEROES[/]");
        tutorialTable2.AddRow("[yellow]Skorin[/]", "[yellow]A guard of the City of Consolas, and an operator of the Cannon of Consolas.[/]", "[yellow]Punch | Cannon of Consolas[/]", "[yellow]HEROES[/]");
        tutorialTable2.AddRow("", "", "", "");
        tutorialTable2.AddRow("[green4]Skeleton[/]", "[green4]A bony, spooky monster that wants nothing more than to steal your flesh.[/]", "[green4]Bone Crunch | Dagger[/]", "[green4]MONSTERS[/]");
        tutorialTable2.AddRow("[dodgerblue3]Stone Amarok[/]", "[dodgerblue3]A stony creature who does NOT like playing rock, paper, scissors.[/]", "[dodgerblue3]Bite | Stone Armor[/]", "[dodgerblue3]MONSTERS[/]");
        tutorialTable2.AddRow("[purple3]Shadow Octopoid[/]", "[purple3]An octopus-like demon who uses all 8 tentacles to attempt to steal your equipped gear[/]", "[purple3]Grapple[/]", "[purple3]MONSTERS[/]");
        tutorialTable2.AddRow("[purple]The Uncoded One[/]", "[purple]The shadowy figure who ripped the lands of programming knowledge 100 years ago. The final boss.[/]", "[purple]Unraveling (Decoding attack)[/]", "[purple]MONSTERS[/]");
        tutorialTable2.AddRow("", "", "", "");

        AnsiConsole.Write(tutorialTable2);
        Console.WriteLine("\n\nPress any key to continue...");

        Console.ReadKey(true);
        Console.Clear();
        pageNum++;
        TutorialHeader(pageNum);
        Table tutorialTable3 = new Table();
        tutorialTable3.Expand();
        tutorialTable3.Border(TableBorder.Rounded);
        tutorialTable3.Title("ITEMS and ABILITIES");
        tutorialTable3.AddColumn("Name");
        tutorialTable3.AddColumn("Description");
        tutorialTable3.AddColumn("Function");
        tutorialTable3.AddRow("[red]Health Potion[/]", "[red]Brewed in the City of Consolas using fruit from Charberry Trees.[/]", "[red]+10 health to user[/]");
        tutorialTable3.AddRow("[lightyellow3]Simula's Soup[/]", "[lightyellow3]A nice, comforting batch of baked potato soup made by your pal Simula![/]", "[lightyellow3]Restores max health to user[/]");
        tutorialTable3.AddRow("[slateblue3]Binary Helm[/]", "[slateblue3]Created by ancient binary programmers, this helm grants protection to it's wearer.[/]", "[slateblue3]-1 damage to user EXCEPT for Piercing Damage[/]");
        tutorialTable3.AddRow("[lightcyan1]Sword[/]", "[lightcyan1]Forged by an elderly blacksmith, this sword has an edge.[/]", "[lightcyan1]2 damage[/]");
        tutorialTable3.AddRow("[grey46]Dagger[/]", "[grey46]A common dagger.[/]", "[grey46]1 damage[/]");
        tutorialTable3.AddRow("[navajowhite1]Vin's Bow[/]", "[navajowhite1]Hand-crafted by Vin Fletcher himself, this bow does Piercing Damage.[/]", "[navajowhite1]50% chance of 1 damage, plus 1 piercing damage over 3 rounds to enemy.[/]");
        tutorialTable3.AddRow("[darkviolet]Cannon of Consolas[/]", "[darkviolet]The mainline defense of the City of Consolas[/]", "[darkviolet]40% chance 1 dmg (Standard Shot), 33% chance 2 dmg (Electrical Shot), 20% chance 3 dmg (Fire Shot), 7% chance 5 dmg (Electrical Fire Shot)[/]");
        tutorialTable3.AddRow("[grey]Stone Armor[/]", "[grey]Some say this armor is grown biologically...[/]", "[grey]-1 damage to user EXCEPT for Piercing Damage[/]");
        tutorialTable3.AddRow("[orangered1]Punch[/]", "[orangered1]Fist, meet face.[/]", "[orangered1]1 damage[/]");
        tutorialTable3.AddRow("[white]Bone Crunch[/]", "[white]Bony fist, meet fleshy face.[/]", "[white]0-1 damage[/]");
        tutorialTable3.AddRow("[darkred]Bite[/]", "[darkred]Om nom[/]", "[darkred]1 damage[/]");
        tutorialTable3.AddRow("[purple_1]Grapple[/]", "[purple_1]A legendary attack that has a chance to steal your equipped item.[/]", "[purple_1]0-1 damage, Stolen Item (25% chance)[/]");
        tutorialTable3.AddRow("[deeppink4_2]U[/][mediumorchid3]N[/][deeppink4_2]R[/][mediumorchid3]A[/][deeppink4_2]V[/][mediumorchid3]E[/][deeppink4_2]L[/][mediumorchid3]I[/][deeppink4_2]N[/][mediumorchid3]G[/]", "[deeppink4_2]The final boss's most powerful attack. Deals considerable decoding damage.[/]", "[deeppink4_2]0-4 damage[/]");
        tutorialTable3.AddRow("[lightyellow3]Object Sight[/]", "[lightyellow3]Harnessed by the True Programmer, Object Sight is a sight to behold.[/]", "[lightyellow3]-2 decoding damage[/]");

        AnsiConsole.Write(tutorialTable3);
        Console.WriteLine("\n\nPress any key to continue...");
        Console.ReadKey(true);
        Console.Clear();
    }

    private static void TutorialHeader(int pageNum)
    {
        Console.Clear();
        Console.ForegroundColor = ConsoleColor.Cyan;
        Console.Write("=====================================================[{ ");
        Console.ForegroundColor = ConsoleColor.Yellow;
        Console.Write($"Page {pageNum}");
        Console.ForegroundColor = ConsoleColor.Cyan;
        Console.Write(" }]=======================================================\n");
        Console.ForegroundColor = ConsoleColor.White;
    }
}