﻿using Spectre.Console;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
public class Character
{
    public int _ID { get; set; }
    public string _name { get; set; }
    public bool _partyManager { get; set; }
    public int _maxHealth { get; set; }
    public int _health { get; set; }
    public Gear _equippedItem { get; set; }
    public AttackModifier _attackModifier { get; set; }
    public TempEffect _tempEffect { get; set; }
    public int _tempEffectRounds { get; set; }

    public Character(int ID, string name, bool isPartyManager, int maxHealth)
    {
        _ID = ID;
        _name = name;
        _partyManager = isPartyManager;
        _maxHealth = maxHealth;
        _health = maxHealth;
    }

    public void DoNothing() // Skip turn
    {
        Console.WriteLine($"{this._name} did NOTHING\n");
        Game.GameDelay();
    }
    
    public void Attack(string attackName, AttackType attackType, int damage, Character enemy, bool isHeroParty)
    {
        int modifier = 0;
        
        //Defensive Attack Modifiers
        AnsiConsole.Write(new Markup($"[steelblue1_1]{_name}[/] used {attackName} on [red]{enemy._name}[/]\n"));
        if (enemy._attackModifier == AttackModifier.StoneArmor)
        {
            modifier += -1;
            AnsiConsole.Write(new Markup("[grey]STONE ARMOR[/] reduced the attack by 1 point.\n"));
        }
        if (enemy._attackModifier == AttackModifier.ObjectSight && attackType == AttackType.Decoding)
        {
            modifier += -2;
            AnsiConsole.Write(new Markup("[slateblue3]OBJECT SIGHT[/] reduced the attack by 2 points.\n"));
        }
        if (enemy._equippedItem == Gear.BinaryHelm)
        {
            modifier += -1;
            AnsiConsole.Write(new Markup("[slateblue3]BINARY HELM[/] reduced the attack by 1 point.\n"));
        }
        if (attackType == AttackType.Grapple)
        {
            if (Game.random.Next(4) == 1) // 1 in 4 chance of stealing Gear
            {
                if (isHeroParty) Game._heroPartyGear.Add(enemy._equippedItem);
                else Game._monsterPartyGear.Add(enemy._equippedItem);
                string equippedItem;
                if (enemy._equippedItem == Gear.CannonofConsolas) equippedItem = "[darkviolet]Cannon of Consolas[/]";
                else if (enemy._equippedItem == Gear.Dagger) equippedItem = "[grey46]Dagger[/]";
                else if (enemy._equippedItem == Gear.Sword) equippedItem = "[lightcyan1]Sword[/]";
                else if (enemy._equippedItem == Gear.Bow) equippedItem = "[navajowhite1]Vin's Bow[/]";
                else if (enemy._equippedItem == Gear.BinaryHelm) equippedItem = "[slateblue3]Binary Helm[/]";
                else equippedItem = "None";
                AnsiConsole.Write(new Markup($"[purple4]GRAPPLE[/] attack stole {equippedItem} from [red]{enemy._name}[/]!\n"));
                enemy._equippedItem = Gear.None;
            }
        }
        if (attackType == AttackType.Piercing)
        {
            enemy._tempEffectRounds = 0;
            enemy._tempEffect = TempEffect.Piercing;
            AnsiConsole.Write(new Markup($"[red]PIERCING DAMAGE[/] done to [red]{enemy._name}[/]! They're bleeding out!\n"));
        }


        if (damage + modifier < 0) damage = 0;
        else damage = damage + modifier; // Make sure enemies can't do negative damage!

        AnsiConsole.Write(new Markup($"{attackName} dealt {damage} damage to [red]{enemy._name}[/]\n"));
        Defend(damage, enemy, isHeroParty);

        // Offensive Attack Modifiers
        if (enemy._attackModifier == AttackModifier.HedgeHog)
        {
            AnsiConsole.Write(new Markup("[orange4]HEDGE HOG[/] caused a counter attack!\n"));
            Defend(1 + modifier, this, true);
        }
        if (isHeroParty) Game._heroXP += damage;
        else Game._monsterXP += damage;
        
        Game.GameDelay();
    }

    public string PickItem(bool isHeroParty)
    {
        List<string> inventoryItems = new List<string>();

        if (isHeroParty)
        {
            foreach (Item item in Game._heroPartyInventory)
            {
                if (item == Item.HealthPotion) inventoryItems.Add("[deeppink4_2]Health Potion[/]");
                if (item == Item.SimulasSoup) inventoryItems.Add("[tan]Simula's Soup[/]");
            }
            foreach (Gear gear in Game._heroPartyGear)
            {
                if (gear == Gear.Dagger) inventoryItems.Add("[grey46]Dagger[/]");
                else if (gear == Gear.Sword) inventoryItems.Add("[lightcyan1]Sword[/]");
                else if (gear == Gear.Bow) inventoryItems.Add("[navajowhite1]Vin's Bow[/]");
                else if (gear == Gear.BinaryHelm) inventoryItems.Add("[slateblue3]Binary Helm[/]");
                else if (gear == Gear.CannonofConsolas) inventoryItems.Add("[darkviolet]Cannon of Consolas[/]");
            }
        }
        else
        {
            foreach (Item item in Game._monsterPartyInventory)
            {
                if (item == Item.HealthPotion) inventoryItems.Add("[deeppink4_2]Health Potion[/]");
                if (item == Item.SimulasSoup) inventoryItems.Add("[tan]Simula's Soup[/]");
            }
            foreach (Gear gear in Game._monsterPartyGear)
            {
                if (gear == Gear.Dagger) inventoryItems.Add("[grey46]Dagger[/]");
                else if (gear == Gear.Sword) inventoryItems.Add("[lightcyan1]Sword[/]");
                else if (gear == Gear.Bow) inventoryItems.Add("[navajowhite1]Vin's Bow[/]");
                else if (gear == Gear.BinaryHelm) inventoryItems.Add("[slateblue3]Binary Helm[/]");
                else if (gear == Gear.CannonofConsolas) inventoryItems.Add("[darkviolet]Cannon of Consolas[/]");
            }
        }

        inventoryItems.Add("Go Back");

        return AnsiConsole.Prompt(
            new SelectionPrompt<string>().Title("[aqua]Available items:[/]")
            .AddChoices(inventoryItems));
    }

    public void EquiptItem(Gear gear, bool isHeroParty)
    {
        if (isHeroParty)
        {
            if (this._equippedItem != Gear.None)
            {
                Game._heroPartyGear.Add(this._equippedItem);
            }
            this._equippedItem = gear;
            Game._heroPartyGear.Remove(gear);
        }
        else
        {
            if (this._equippedItem != Gear.None)
            {
                Game._monsterPartyGear.Add(this._equippedItem);
            }
            this._equippedItem = gear;
            Game._monsterPartyGear.Remove(gear);
        }
        string gearName;
        if (gear == Gear.CannonofConsolas) gearName = "[darkviolet]Cannon of Consolas[/]";
        else if (gear == Gear.Dagger) gearName = "[grey46]Dagger[/]";
        else if (gear == Gear.Sword) gearName = "[lightcyan1]Sword[/]";
        else if (gear == Gear.Bow) gearName = "[navajowhite1]Vin's Bow[/]";
        else if (gear == Gear.BinaryHelm) gearName = "[slateblue3]Binary Helm[/]";
        else gearName = ""; // Should not be set
        AnsiConsole.Write(new Markup($"[steelblue1_1]{_name}[/] equipped {gearName}!\n\n"));
        Game.GameDelay();

    }

    public virtual void Action(string action, Character enemy)
    {
        throw new NotImplementedException(); // Should not be called, sub classes have overrides.
    }

    public virtual void Reset()
    {
        throw new NotImplementedException(); // Should not be called, sub classes have overrides.
    }

    public virtual string GetChoice()
    {
        throw new NotImplementedException(); // Should not be called, sub classes have overrides.
    }

    public void Defend(int damage, Character enemy, bool isHeroParty)
    {
        if (enemy._health > 0) enemy._health -= damage;
        if (enemy._health <= 0)
        {
            enemy._health = 0;
            AnsiConsole.Write(new Markup($"[red]{enemy._name}[/] has been defeated!\n"));
            if (isHeroParty)
            {
                if (enemy._name == "THE UNCODED ONE") Game._heroXP += 30;
                if (enemy._name == "SKELETON") Game._heroXP += 5;
                if (enemy._name == "STONE AMAROK") Game._heroXP += 7;
                Game._monsterParty.Remove(enemy);
            }
            else
            {
                if (enemy._ID == 0) Game._monsterXP += 30;
                if (enemy._name == "SKORIN") Game._monsterXP += 10;
                if (enemy._name == "VIN FLETCHER") Game._monsterXP += 17;
                Game._heroParty.Remove(enemy);
            }
        }
        if (enemy._health > enemy._maxHealth) enemy._health = enemy._maxHealth;
        AnsiConsole.Write(new Markup($"[red]{enemy._name}[/] is now at [red]{enemy._health}/{enemy._maxHealth}[/] HP.\n\n"));
    }

}

public class StoneAmarok : Character
{
    public StoneAmarok(int ID, string name, bool isPartyManager) : base(ID, name, isPartyManager, 4)
    {

    }

    public override void Action(string action, Character enemy)
    {
        switch (action)
        {
            case "Do Nothing":
                DoNothing();
                break;
            case "Standard Attack (BITE)":
                Attack("[red1]BITE[/]", AttackType.Normal, 1, enemy, false);
                break;
            case "Gear Attack (Dagger)":
                Attack("[grey46]DAGGER SHANK[/]", AttackType.Normal, 1, enemy, false);
                break;
            case "Gear Attack (Sword)":
                Attack("[lightcyan1]SWORD SLASH[/]", AttackType.Normal, 2, enemy, false);
                break;
            case "Gear Attack (Vin's Bow)":
                if (Game.random.Next(2) == 1)
                {
                    Attack("[navajowhite1]VIN'S BOW[/]", AttackType.Piercing, 1, enemy, false);
                    break;
                }
                else
                {
                    AnsiConsole.Write(new Markup($"[steelblue1_1]{_name}[/] used [navajowhite1]VIN'S BOW[/] on [red]{enemy._name}[/]!\n"));
                    AnsiConsole.Write(new Markup($"[steelblue1_1]{_name}[/] MISSED!\n\n"));
                    Game.GameDelay();
                    break;
                }
            case "Gear Attack (Cannon of Consolas)":
                int cannonFire = Game.random.Next(1, 101);
                if (cannonFire % 3 == 0 && cannonFire % 5 == 0) Attack("[orange3]ELECTRICAL FIRE CANNON SHOT[/]", AttackType.Normal, 5, enemy, false);
                else if (cannonFire % 3 == 0) Attack("[yellow3_1]ELECTRICAL CANNON SHOT[/]", AttackType.Normal, 2, enemy, false);
                else if (cannonFire % 5 == 0) Attack("[maroon]FIRE CANNON SHOT[/]", AttackType.Normal, 3, enemy, false);
                else Attack("REGULAR CANNON SHOT", AttackType.Normal, 1, enemy, false);
                break;
            case "Use Item":
                string item = PickItem(false);
                if (item == "[deeppink4_2]Health Potion[/]")
                {
                    AnsiConsole.Write(new Markup($"[steelblue1_1]{_name}[/] used {item} on self!\n"));
                    Defend(-10, this, false);
                    Game._monsterPartyInventory.Remove(Item.HealthPotion);
                    Game.GameDelay();
                }
                else if (item == "[tan]Simula's Soup[/]")
                {
                    AnsiConsole.Write(new Markup($"[steelblue1_1]{_name}[/] used {item} on self!\n\n"));
                    _health = _maxHealth;
                    Game._monsterPartyInventory.Remove(Item.SimulasSoup);
                    Game.GameDelay();
                }
                else if (item == "[grey46]Dagger[/]")
                {
                    EquiptItem(Gear.Dagger, false);
                }
                else if (item == "[lightcyan1]Sword[/]")
                {
                    EquiptItem(Gear.Sword, false);
                }
                else if (item == "[navajowhite1]Vin's Bow[/]")
                {
                    EquiptItem(Gear.Bow, false);
                }
                else if (item == "[slateblue3]Binary Helm[/]")
                {
                    EquiptItem(Gear.BinaryHelm, false);
                }
                else
                {
                    Action(GetChoice(), enemy);
                }
                break;
            case "Used Item":
                Game.GameDelay();
                break;
            default:
                DoNothing();
                break;
        }
    }

    public override void Reset()
    {
        _health = _maxHealth;
        _equippedItem = Gear.None;
    }

    public override string GetChoice()
    {
        if (_equippedItem == Gear.None || _equippedItem == Gear.BinaryHelm)
        {
            return AnsiConsole.Prompt(
            new SelectionPrompt<string>()
            .Title("What would you like to do?")
            .AddChoices(new[]
            {
                "Standard Attack (BITE)", "Use Item", "Do Nothing"
            }));
        }
        else
        {
            string equippedItem;
            if (_equippedItem == Gear.CannonofConsolas) equippedItem = "Cannon of Consolas";
            else if (_equippedItem == Gear.Dagger) equippedItem = "Dagger";
            else if (_equippedItem == Gear.Sword) equippedItem = "Sword";
            else equippedItem = "None"; // Should never be none.

            return AnsiConsole.Prompt(
            new SelectionPrompt<string>()
            .Title("What would you like to do?")
            .AddChoices(new[]
            {
                "Standard Attack (BITE)", $"Gear Attack ({equippedItem})", "Use Item", "Do Nothing"
            }));
        }
    }
}
public class Skeleton : Character
{
    Random skeletonRandom = new Random();
    public Skeleton(int ID, string name, bool isPartyManager) : base(ID, name, isPartyManager, 5)
    {

    }

    public override void Action(string action, Character enemy)
    {
        switch (action)
        {
            case "Do Nothing":
                DoNothing();
                break;
            case "Standard Attack (BONE CRUNCH)":
                Attack("[grey70]BONE CRUNCH[/]", AttackType.Normal, skeletonRandom.Next(2), enemy, false);
                break;
            case "Gear Attack (Dagger)":
                Attack("[grey46]DAGGER SHANK[/]", AttackType.Normal, 1, enemy, false);
                break;
            case "Gear Attack (Sword)":
                Attack("[lightcyan1]SWORD SLASH[/]", AttackType.Normal, 2, enemy, false);
                break;
            case "Gear Attack (Vin's Bow)":
                if (Game.random.Next(2) == 1)
                {
                    Attack("[navajowhite1]VIN'S BOW[/]", AttackType.Piercing, 1, enemy, false);
                    break;
                }
                else
                {
                    AnsiConsole.Write(new Markup($"[steelblue1_1]{_name}[/] used [navajowhite1]VIN'S BOW[/] on [red]{enemy._name}[/]!\n"));
                    AnsiConsole.Write(new Markup($"[steelblue1_1]{_name}[/] MISSED!\n\n"));
                    Game.GameDelay();
                    break;
                }
            case "Gear Attack (Cannon of Consolas)":
                int cannonFire = Game.random.Next(1, 101);
                if (cannonFire % 3 == 0 && cannonFire % 5 == 0) Attack("[orange3]ELECTRICAL FIRE CANNON SHOT[/]", AttackType.Normal, 5, enemy, false);
                else if (cannonFire % 3 == 0) Attack("[yellow3_1]ELECTRICAL CANNON SHOT[/]", AttackType.Normal, 2, enemy, false);
                else if (cannonFire % 5 == 0) Attack("[maroon]FIRE CANNON SHOT[/]", AttackType.Normal, 3, enemy, false);
                else Attack("REGULAR CANNON SHOT", AttackType.Normal, 1, enemy, false);
                break;
            case "Use Item":
                string item = PickItem(false);
                if (item == "[deeppink4_2]Health Potion[/]")
                {
                    AnsiConsole.Write(new Markup($"[steelblue1_1]{_name}[/] used {item} on self!\n"));
                    Defend(-10, this, false);
                    Game._monsterPartyInventory.Remove(Item.HealthPotion);
                    Game.GameDelay();
                }
                else if (item == "[tan]Simula's Soup[/]")
                {
                    AnsiConsole.Write(new Markup($"[steelblue1_1]{_name}[/] used {item} on self!\n"));
                    _health = _maxHealth;
                    Game._monsterPartyInventory.Remove(Item.SimulasSoup);
                    Game.GameDelay();
                }
                else if (item == "[grey46]Dagger[/]")
                {
                    EquiptItem(Gear.Dagger, false);
                }
                else if (item == "[lightcyan1]Sword[/]")
                {
                    EquiptItem(Gear.Sword, false);
                }
                else if (item == "[navajowhite1]Vin's Bow[/]")
                {
                    EquiptItem(Gear.Bow, false);
                }
                else if (item == "[slateblue3]Binary Helm[/]")
                {
                    EquiptItem(Gear.BinaryHelm, false);
                }
                else
                {
                    Action(GetChoice(), enemy);
                }
                break;
            case "Used Item":
                Game.GameDelay();
                break;
            default:
                DoNothing();
                break;
        }
    }

    public override void Reset()
    {
        _health = _maxHealth;
        _equippedItem = Gear.None;
    }

    public override string GetChoice()
    {
        if (_equippedItem == Gear.None || _equippedItem == Gear.BinaryHelm)
        {
            return AnsiConsole.Prompt(
            new SelectionPrompt<string>()
            .Title("What would you like to do?")
            .AddChoices(new[]
            {
                "Standard Attack (BONE CRUNCH)", "Use Item", "Do Nothing"
            }));
        }
        else
        {
            string equippedItem;
            if (_equippedItem == Gear.CannonofConsolas) equippedItem = "Cannon of Consolas";
            else if (_equippedItem == Gear.Dagger) equippedItem = "Dagger";
            else if (_equippedItem == Gear.Sword) equippedItem = "Sword";
            else equippedItem = "None"; // Should never be none.

            return AnsiConsole.Prompt(
            new SelectionPrompt<string>()
            .Title("What would you like to do?")
            .AddChoices(new[]
            {
                "Standard Attack (BONE CRUNCH)", $"Gear Attack ({equippedItem})", "Use Item", "Do Nothing"
            }));
        }
    }
}

public class Human : Character
{
    public Human(int ID, string name, bool isPartyManager, int maxHealth) : base(ID, name, isPartyManager, maxHealth)
    {

    }
    public override void Reset()
    {
        _health = _maxHealth;
    }
    public override void Action(string action, Character enemy)
    {
        switch (action)
        {
            case "Do Nothing":
                DoNothing();
                break;
            case "Standard Attack (PUNCH)":
                Attack("[lightcoral]PUNCH[/]", AttackType.Normal, 1, enemy, true);
                break;
            case "Gear Attack (Dagger)":
                Attack("[grey46]DAGGER SHANK[/]", AttackType.Normal, 1, enemy, true);
                break;
            case "Gear Attack (Sword)":
                Attack("[lightcyan1]SWORD SLASH[/]", AttackType.Normal, 2, enemy, true);
                break;
            case "Gear Attack (Vin's Bow)":
                if (Game.random.Next(2) == 1)
                {
                    Attack("[navajowhite1]VIN'S BOW[/]", AttackType.Piercing, 1, enemy, true);
                    break;
                }
                else
                {
                    AnsiConsole.Write(new Markup($"[steelblue1_1]{_name}[/] used [navajowhite1]VIN'S BOW[/] on [red]{enemy._name}[/]!\n"));
                    AnsiConsole.Write(new Markup($"[steelblue1_1]{_name}[/] MISSED!\n\n"));
                    Game.GameDelay();
                    break;
                }
            case "Gear Attack (Cannon of Consolas)":
                int cannonFire = Game.random.Next(1, 101);
                if (cannonFire % 3 == 0 && cannonFire % 5 == 0) Attack("[orange3]ELECTRICAL FIRE CANNON SHOT[/]", AttackType.Normal, 5, enemy, true);
                else if (cannonFire % 3 == 0) Attack("[yellow3_1]ELECTRICAL CANNON SHOT[/]", AttackType.Normal, 2, enemy, true);
                else if (cannonFire % 5 == 0) Attack("[maroon]FIRE CANNON SHOT[/]", AttackType.Normal, 3, enemy, true);
                else Attack("REGULAR CANNON SHOT", AttackType.Normal, 1, enemy, true);
                break;
            case "Use Item":
                string item = PickItem(true);
                if (item == "[deeppink4_2]Health Potion[/]")
                {
                    AnsiConsole.Write(new Markup($"[steelblue1_1]{_name}[/] used [deeppink4_2]{item}[/] on self!\n"));
                    Defend(-10, this, true);
                    Game._heroPartyInventory.Remove(Item.HealthPotion);
                    Game.GameDelay();
                }
                else if (item == "[tan]Simula's Soup[/]")
                {
                    AnsiConsole.Write(new Markup($"[steelblue1_1]{_name}[/] used {item} on self!\n"));
                    _health = _maxHealth;
                    Game._heroPartyInventory.Remove(Item.SimulasSoup);
                    Game.GameDelay();
                }
                else if (item == "[grey46]Dagger[/]")
                {
                    EquiptItem(Gear.Dagger, true);
                }
                else if (item == "[lightcyan1]Sword[/]")
                {
                    EquiptItem(Gear.Sword, true);
                }
                else if (item == "[navajowhite1]Vin's Bow[/]")
                {
                    EquiptItem(Gear.Bow, true);
                }
                else if (item == "[slateblue3]Binary Helm[/]")
                {
                    EquiptItem(Gear.BinaryHelm, true);
                }
                else if (item == "[darkviolet]Cannon of Consolas[/]")
                {
                    EquiptItem(Gear.CannonofConsolas, true);
                }
                else
                {
                    Action(GetChoice(), enemy);
                }
                break;
            case "Used Item":
                Game.GameDelay();
                break;
            default:
                Console.WriteLine("Uh oh! That wasn't supposed to happen...");
                DoNothing();
                break;
        }
    }
}

public class TrueProgrammer : Human
{
    public TrueProgrammer(int ID, string name, bool isPartyManager) : base(ID, name, isPartyManager, 25)
    {

    }
    public override string GetChoice()
    {
        if (_equippedItem == Gear.None || _equippedItem == Gear.BinaryHelm)
        {
            return AnsiConsole.Prompt(
            new SelectionPrompt<string>()
            .Title("What would you like to do?")
            .AddChoices(new[]
            {
                "Standard Attack (PUNCH)", "Use Item", "Do Nothing"
            }));
        }
        else
        {
            string equippedItem;
            if (_equippedItem == Gear.CannonofConsolas) equippedItem = "Cannon of Consolas";
            else if (_equippedItem == Gear.Dagger) equippedItem = "Dagger";
            else if (_equippedItem == Gear.Sword) equippedItem = "Sword";
            else if (_equippedItem == Gear.Bow) equippedItem = "Vin's Bow";
            else equippedItem = "None"; // Should never be none.

            return AnsiConsole.Prompt(
            new SelectionPrompt<string>()
            .Title("What would you like to do?")
            .AddChoices(new[]
            {
                "Standard Attack (PUNCH)", $"Gear Attack ({equippedItem})", "Use Item", "Do Nothing"
            }));
        }
    }
}

public class VinFletcher : Human
{
    public VinFletcher(int ID, string name, bool isPartyManager) : base(ID, name, isPartyManager, 15)
    {

    }
    public override string GetChoice()
    {
        if (_equippedItem == Gear.None || _equippedItem == Gear.BinaryHelm)
        {
            return AnsiConsole.Prompt(
            new SelectionPrompt<string>()
            .Title("What would you like to do?")
            .AddChoices(new[]
            {
                "Standard Attack (PUNCH)", "Use Item", "Do Nothing"
            }));
        }
        else
        {
            string equippedItem;
            if (_equippedItem == Gear.CannonofConsolas) equippedItem = "Cannon of Consolas";
            else if (_equippedItem == Gear.Dagger) equippedItem = "Dagger";
            else if (_equippedItem == Gear.Sword) equippedItem = "Sword";
            else if (_equippedItem == Gear.Bow) equippedItem = "Vin's Bow";
            else equippedItem = "None"; // Should never be none.

            return AnsiConsole.Prompt(
            new SelectionPrompt<string>()
            .Title("What would you like to do?")
            .AddChoices(new[]
            {
                "Standard Attack (PUNCH)", $"Gear Attack ({equippedItem})", "Use Item", "Do Nothing"
            }));
        }
    }
}

public class Skorin : Human
{
    public Skorin(int ID, string name, bool isPartyManager) : base(ID, name, isPartyManager, 10)
    {

    }
    public override string GetChoice()
    {
        if (_equippedItem == Gear.None || _equippedItem == Gear.BinaryHelm)
        {
            return AnsiConsole.Prompt(
            new SelectionPrompt<string>()
            .Title("What would you like to do?")
            .AddChoices(new[]
            {
                "Standard Attack (PUNCH)", "Use Item", "Do Nothing"
            }));
        }
        else
        {
            string equippedItem;
            if (_equippedItem == Gear.CannonofConsolas) equippedItem = "Cannon of Consolas";
            else if (_equippedItem == Gear.Dagger) equippedItem = "Dagger";
            else if (_equippedItem == Gear.Sword) equippedItem = "Sword";
            else if (_equippedItem == Gear.Bow) equippedItem = "Vin's Bow";
            else equippedItem = "None"; // Should never be none.

            return AnsiConsole.Prompt(
            new SelectionPrompt<string>()
            .Title("What would you like to do?")
            .AddChoices(new[]
            {
                "Standard Attack (PUNCH)", $"Gear Attack ({equippedItem})", "Use Item", "Do Nothing"
            }));
        }
    }
}

public class TheUncodedOne : Character
{
    public TheUncodedOne(int ID, string name, bool isPartyManager) : base(ID, name, isPartyManager, 30)
    {

    }
    public override void Action(string action, Character enemy)
    {
        switch (action)
        {
            case "Do Nothing":
                DoNothing();
                break;
            case "Standard Attack (UNRAVELING)":
                Attack("[deeppink4_2]U[/][mediumorchid3]N[/][deeppink4_2]R[/][mediumorchid3]A[/][deeppink4_2]V[/][mediumorchid3]E[/][deeppink4_2]L[/][mediumorchid3]I[/][deeppink4_2]N[/][mediumorchid3]G[/]", AttackType.Decoding, Game.random.Next(5), enemy, false);
                break;
            case "Gear Attack (Dagger)":
                Attack("[grey46]DAGGER SHANK[/]", AttackType.Normal, 1, enemy, false);
                break;
            case "Gear Attack (Sword)":
                Attack("[lightcyan1]SWORD SLASH[/]", AttackType.Normal, 2, enemy, false);
                break;
            case "Gear Attack (Vin's Bow)":
                if (Game.random.Next(2) == 1)
                {
                    Attack("[navajowhite1]VIN'S BOW[/]", AttackType.Piercing, 1, enemy, false);
                    break;
                }
                else
                {
                    AnsiConsole.Write(new Markup($"[steelblue1_1]{_name}[/] used [navajowhite1]VIN'S BOW[/] on [red]{enemy._name}[/]!\n"));
                    AnsiConsole.Write(new Markup($"[steelblue1_1]{_name}[/] MISSED!\n\n"));
                    Game.GameDelay();
                    break;
                }
            case "Gear Attack (Cannon of Consolas)":
                int cannonFire = Game.random.Next(1, 101);
                if (cannonFire % 3 == 0 && cannonFire % 5 == 0) Attack("[orange3]ELECTRICAL FIRE CANNON SHOT[/]", AttackType.Normal, 5, enemy, false);
                else if (cannonFire % 3 == 0) Attack("[yellow3_1]ELECTRICAL CANNON SHOT[/]", AttackType.Normal, 2, enemy, false);
                else if (cannonFire % 5 == 0) Attack("[maroon]FIRE CANNON SHOT[/]", AttackType.Normal, 3, enemy, false);
                else Attack("REGULAR CANNON SHOT", AttackType.Normal, 1, enemy, false);
                break;
            case "Use Item":
                string item = PickItem(false);
                if (item == "[deeppink4_2]Health Potion[/]")
                {
                    AnsiConsole.Write(new Markup($"[steelblue1_1]{_name}[/] used [deeppink4_2]{item}[/] on self!\n"));
                    Defend(-10, this, false);
                    Game._monsterPartyInventory.Remove(Item.HealthPotion);
                    Game.GameDelay();
                }
                else if (item == "[tan]Simula's Soup[/]")
                {
                    AnsiConsole.Write(new Markup($"[steelblue1_1]{_name}[/] used {item} on self!\n"));
                    _health = _maxHealth;
                    Game._monsterPartyInventory.Remove(Item.SimulasSoup);
                    Game.GameDelay();
                }
                else if (item == "[grey46]Dagger[/]")
                {
                    EquiptItem(Gear.Dagger, false);
                }
                else if (item == "[lightcyan1]Sword[/]")
                {
                    EquiptItem(Gear.Sword, false);
                }
                else if (item == "[navajowhite1]Vin's Bow[/]")
                {
                    EquiptItem(Gear.Bow, false);
                }
                else if (item == "[slateblue3]Binary Helm[/]")
                {
                    EquiptItem(Gear.BinaryHelm, false);
                }
                else
                {
                    Action(GetChoice(), enemy);
                }
                break;
            case "Used Item":
                Game.GameDelay();
                break;
            default:
                DoNothing();
                break;
        }
    }
    public override void Reset()
    {
        _health = _maxHealth;
    }
    public override string GetChoice()
    {
        if (_equippedItem == Gear.None || _equippedItem == Gear.BinaryHelm)
        {
            return AnsiConsole.Prompt(
            new SelectionPrompt<string>()
            .Title("What would you like to do?")
            .AddChoices(new[]
            {
                "Standard Attack (UNRAVELING)", "Use Item", "Do Nothing"
            }));
        }
        else
        {
            string equippedItem;
            if (_equippedItem == Gear.CannonofConsolas) equippedItem = "Cannon of Consolas";
            else if (_equippedItem == Gear.Dagger) equippedItem = "Dagger";
            else if (_equippedItem == Gear.Sword) equippedItem = "Sword";
            else equippedItem = "None"; // Should never be none.

            return AnsiConsole.Prompt(
            new SelectionPrompt<string>()
            .Title("What would you like to do?")
            .AddChoices(new[]
            {
                "Standard Attack (UNRAVELING)", $"Gear Attack ({equippedItem})", "Use Item", "Do Nothing"
            }));
        }
    }
}

public class ShadowOctopoid : Character
{
    public ShadowOctopoid(int ID, string name, bool isPartyManager) : base(ID, name, isPartyManager, 15)
    {

    }
    public override void Action(string action, Character enemy)
    {
        switch (action)
        {
            case "Do Nothing":
                DoNothing();
                break;
            case "Standard Attack (GRAPPLE)":
                Attack("[purple4]GRAPPLE[/]", AttackType.Grapple, Game.random.Next(2), enemy, false);
                break;
            case "Gear Attack (Dagger)":
                Attack("[grey46]DAGGER SHANK[/]", AttackType.Normal, 1, enemy, false);
                break;
            case "Gear Attack (Sword)":
                Attack("[lightcyan1]SWORD SLASH[/]", AttackType.Normal, 2, enemy, false);
                break;
            case "Gear Attack (Vin's Bow)":
                if (Game.random.Next(2) == 1)
                {
                    Attack("[navajowhite1]VIN'S BOW[/]", AttackType.Piercing, 1, enemy, false);
                    break;
                }
                else
                {
                    AnsiConsole.Write(new Markup($"[steelblue1_1]{_name}[/] used [navajowhite1]VIN'S BOW[/] on [red]{enemy._name}[/]!\n"));
                    AnsiConsole.Write(new Markup($"[steelblue1_1]{_name}[/] MISSED!\n\n"));
                    Game.GameDelay();
                    break;
                }
            case "Gear Attack (Cannon of Consolas)":
                int cannonFire = Game.random.Next(1, 101);
                if (cannonFire % 3 == 0 && cannonFire % 5 == 0) Attack("[orange3]ELECTRICAL FIRE CANNON SHOT[/]", AttackType.Normal, 5, enemy, false);
                else if (cannonFire % 3 == 0) Attack("[yellow3_1]ELECTRICAL CANNON SHOT[/]", AttackType.Normal, 2, enemy, false);
                else if (cannonFire % 5 == 0) Attack("[maroon]FIRE CANNON SHOT[/]", AttackType.Normal, 3, enemy, false);
                else Attack("REGULAR CANNON SHOT", AttackType.Normal, 1, enemy, false);
                break;
            case "Use Item":
                string item = PickItem(false);
                if (item == "[deeppink4_2]Health Potion[/]")
                {
                    AnsiConsole.Write(new Markup($"[steelblue1_1]{_name}[/] used [deeppink4_2]{item}[/] on self!\n"));
                    Defend(-10, this, false);
                    Game._monsterPartyInventory.Remove(Item.HealthPotion);
                    Game.GameDelay();
                }
                else if (item == "[tan]Simula's Soup[/]")
                {
                    AnsiConsole.Write(new Markup($"[steelblue1_1]{_name}[/] used {item} on self!\n"));
                    _health = _maxHealth;
                    Game._monsterPartyInventory.Remove(Item.SimulasSoup);
                    Game.GameDelay();
                }
                else if (item == "[grey46]Dagger[/]")
                {
                    EquiptItem(Gear.Dagger, false);
                }
                else if (item == "[lightcyan1]Sword[/]")
                {
                    EquiptItem(Gear.Sword, false);
                }
                else if (item == "[navajowhite1]Vin's Bow[/]")
                {
                    EquiptItem(Gear.Bow, false);
                }
                else if (item == "[slateblue3]Binary Helm[/]")
                {
                    EquiptItem(Gear.BinaryHelm, false);
                }
                else
                {
                    Action(GetChoice(), enemy);
                }
                break;
            case "Used Item":
                Game.GameDelay();
                break;
            default:
                DoNothing();
                break;
        }
    }
    public override void Reset()
    {
        _health = _maxHealth;
    }
    public override string GetChoice()
    {
        if (_equippedItem == Gear.None || _equippedItem == Gear.BinaryHelm)
        {
            return AnsiConsole.Prompt(
            new SelectionPrompt<string>()
            .Title("What would you like to do?")
            .AddChoices(new[]
            {
                "Standard Attack (GRAPPLE)", "Use Item", "Do Nothing"
            }));
        }
        else
        {
            string equippedItem;
            if (_equippedItem == Gear.CannonofConsolas) equippedItem = "Cannon of Consolas";
            else if (_equippedItem == Gear.Dagger) equippedItem = "Dagger";
            else if (_equippedItem == Gear.Sword) equippedItem = "Sword";
            else equippedItem = "None"; // Should never be none.

            return AnsiConsole.Prompt(
            new SelectionPrompt<string>()
            .Title("What would you like to do?")
            .AddChoices(new[]
            {
                "Standard Attack (GRAPPLE)", $"Gear Attack ({equippedItem})", "Use Item", "Do Nothing"
            }));
        }
    }
}