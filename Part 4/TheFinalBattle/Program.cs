﻿using Spectre.Console;
using Spectre.Console.Cli;

// The Final Battle

Console.Title = "The Final Battle, by Serinity";
Console.WindowHeight = 35;

Game.DisplayTutorial();

Skeleton skeleton1 = new Skeleton(1, "SKELETON", true);
Skeleton skeleton2 = new Skeleton(2, "SKELETON", true);
TheUncodedOne theUncodedOne = new TheUncodedOne(3, "THE UNCODED ONE", true);
StoneAmarok stoneAmarok1 = new StoneAmarok(5, "STONE AMAROK", false);
StoneAmarok stoneAmarok2 = new StoneAmarok(6, "STONE AMAROK", false);
ShadowOctopoid shadowOctopoid = new ShadowOctopoid(7, "SHADOW OCTOPOID", false);
string? input;

while (true)
{
    Console.WriteLine("What is your name, great True Programmer?");
    input = Console.ReadLine();
    if (input != null && input != "" && input != " ")
    {
        break;
    }
    else
    {
        Console.Clear();
        Console.WriteLine("That's not a valid name...");
        continue;
    }
}

TrueProgrammer trueProgrammer = new TrueProgrammer(0, input.ToUpper(), true);
VinFletcher vinFletcher = new VinFletcher(4, "VIN FLETCHER", false);
Skorin skorin = new Skorin(7, "SKORIN", false);
Console.Clear();

input = AnsiConsole.Prompt(
    new SelectionPrompt<string>()
    .Title("Who's playing?")
    .AddChoices(new[]
    {
        "Player vs. Computer", "Player vs. Player", "Computer vs. Computer"
    }));

switch (input)
{
    case "Player vs. Computer":
        Game._gameMode = GameMode.PlayerVComputer; break;
    case "Player vs. Player":
        Game._gameMode = GameMode.PlayerVPlayer; break;
    case "Computer vs. Computer":
        Game._gameMode = GameMode.ComputerVComputer; break;
    default:
        Game._gameMode = GameMode.PlayerVComputer; break;
}

if (Game._gameMode == GameMode.ComputerVComputer || Game._gameMode == GameMode.PlayerVComputer)
{
    input = AnsiConsole.Prompt(
    new SelectionPrompt<string>()
    .Title("How fast do you want the computer to run?")
    .AddChoices(new[]
    {
        "Manual", "Slow", "Medium", "Fast"
    }));

    switch (input)
    {
        case "Manual":
            Game._gameSpeed = GameSpeed.Manual; break;
        case "Slow":
            Game._gameSpeed = GameSpeed.Slow; break;
        case "Medium":
            Game._gameSpeed = GameSpeed.Medium; break;
        case "Fast":
            Game._gameSpeed = GameSpeed.Fast; break;
        default:
            break;
    }
}


Character heroPartyManager;
Character monsterPartyManager;

// Begin Level 1


Game._heroParty.Add(trueProgrammer);
trueProgrammer._attackModifier = AttackModifier.ObjectSight;
Game._heroParty.Add(vinFletcher);
Game._heroParty.Add(skorin);
skorin._attackModifier = AttackModifier.HedgeHog;
Game._heroPartyInventory.Add(Item.HealthPotion);
Game._heroPartyGear.Add(Gear.BinaryHelm);
trueProgrammer._equippedItem = Gear.Sword;
vinFletcher._equippedItem = Gear.Bow;
skorin._equippedItem = Gear.CannonofConsolas;
Game._monsterParty.Add(skeleton1);
skeleton1._equippedItem = Gear.Dagger;
Game._monsterPartyInventory.Add(Item.HealthPotion);

Game._round = 1;
Game.DisplayLevelScreen();

for (int i = 0; i < Game._heroParty.Count; i++)
{
    if (Game._heroParty[i]._partyManager == true) heroPartyManager = Game._heroParty[i]; // Find and set hero party's manager
}

for (int i = 0; i < Game._monsterParty.Count; i++)
{
    if (Game._monsterParty[i]._partyManager == true) monsterPartyManager = Game._monsterParty[i]; // Find and set monster party's manager
}

PlayGame();

// Begin Level 2

skeleton1.Reset();
Game._monsterParty.Add(skeleton1);
Game._monsterParty.Add(skeleton2);
Game._monsterPartyInventory.Clear();
Game._monsterPartyGear.Add(Gear.Dagger);
Game._monsterPartyGear.Add(Gear.Dagger);
Game._monsterPartyGear.Add(Gear.BinaryHelm);
Game._monsterPartyInventory.Add(Item.HealthPotion);
Game._monsterPartyInventory.Add(Item.SimulasSoup);

Game._round = 2;
Game.DisplayLevelScreen();

PlayGame();

// Begin Level 3
Game._monsterParty.Add(stoneAmarok1);
Game._monsterParty.Add(stoneAmarok2);
stoneAmarok1._attackModifier = AttackModifier.StoneArmor;
stoneAmarok2._attackModifier = AttackModifier.StoneArmor;
Game._monsterPartyInventory.Clear();
Game._monsterPartyInventory.Add(Item.HealthPotion);

Game._round = 3;
Game.DisplayLevelScreen();

PlayGame();

// Begin Level 4

skeleton1.Reset();
skeleton2.Reset();
Game._monsterParty.Add(theUncodedOne);
Game._monsterParty.Add(skeleton1);
Game._monsterParty.Add(skeleton2);
Game._monsterParty.Add(shadowOctopoid);
Game._monsterPartyInventory.Clear();
Game._monsterPartyInventory.Add(Item.HealthPotion);
Game._monsterPartyInventory.Add(Item.SimulasSoup);

Game._round = 4;
Game.DisplayLevelScreen();

PlayGame();

static void PlayGame()
{
    while (true)
    {
        Game.PrintGame(true);
        Game.HeroPartyTurn();
        if (Game._monsterParty.Count == 0 && Game._round == 4) Game.WinState();
        else if (Game._monsterParty.Count == 0) // Steal monster party's uneqipped gear and unused items
        {
            foreach (Gear gear in Game._monsterPartyGear)
            {
                Game._heroPartyGear.Add(gear);
                if (gear == Gear.Dagger) AnsiConsole.Write(new Markup("[grey46]Dagger[/] stolen from Monster Party!\n"));
                else if (gear == Gear.Sword) AnsiConsole.Write(new Markup("[lightcyan1]Sword[/] stolen from Monster Party!\n"));
                else if (gear == Gear.Bow) AnsiConsole.Write(new Markup("[navajowhite1]Vin Fletcher's Bow[/] stolen from Monster Party!\n"));
                else if (gear == Gear.CannonofConsolas) AnsiConsole.Write(new Markup("[darkviolet]Cannon of Consolas[/] stolen from Monster Party!\n"));
                else if (gear == Gear.CannonofConsolas) AnsiConsole.Write(new Markup("[slateblue3]Binary Helm[/] stolen from Monster Party!\n"));
            }
            Game._monsterPartyGear.Clear();
            foreach (Item item in Game._monsterPartyInventory)
            {
                Game._heroPartyInventory.Add(item);
                if (item == Item.HealthPotion) AnsiConsole.Write(new Markup("[deeppink4_2]Health Potion[/] stolen from Monster Party!\n"));
                if (item == Item.SimulasSoup) AnsiConsole.Write(new Markup("[tan]Simula's Soup[/] stolen from Monster Party!"));
            }
            Game._monsterPartyInventory.Clear();
            Game.GameDelay();
            break;
        }
            

        Game.PrintGame(false);
        Game.MonsterPartyTurn();
        if (Game._heroParty.Count == 0) Game.WinState();
    }
}

public enum GameMode { ComputerVComputer, PlayerVComputer, PlayerVPlayer }

public enum GameSpeed { Slow, Medium, Fast, Manual }

public enum Item { HealthPotion, SimulasSoup }

public enum Gear { None, Sword, Dagger, Bow, CannonofConsolas, BinaryHelm }

public enum AttackModifier { None, StoneArmor, ObjectSight, HedgeHog }

public enum AttackType { Normal, Decoding, Grapple, Piercing }

public enum TempEffect { None, Piercing }