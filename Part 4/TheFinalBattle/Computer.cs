﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Spectre.Console;
using Spectre.Console.Cli;

public static class ComputerController
{
    public static string GetRandomAction(Character character)
    {
        if (character._name == "THE UNCODED ONE")
        {
            if (Game._monsterPartyInventory.Contains<Item>(Item.SimulasSoup) && character._health < character._maxHealth / 2 && Game.random.Next(4) == 0)
            {
                AnsiConsole.Write(new Markup($"[steelblue1_1]{character._name}[/] used [tan]Simula's Soup[/] on self!\n"));
                character._health = character._maxHealth;
                Game._monsterPartyInventory.Remove(Item.SimulasSoup);
                return "Used Item";
            }
            if (Game._monsterPartyInventory.Contains<Item>(Item.HealthPotion) && character._health < character._maxHealth / 2 && Game.random.Next(4) == 0)
            {
                character.Defend(-10, character, false);
                Game._monsterPartyInventory.Remove(Item.HealthPotion);
                AnsiConsole.Write(new Markup($"[steelblue1_1]{character._name}[/] used [red]Health Potion[/] on self!\n\n"));
                return "Used Item";
            }

            string equippedItem;
            equippedItem = GetFriendlyItemName(character);

            if (Game._monsterPartyGear.Count > 0 && Game.random.Next(2) == 1 && character._equippedItem == Gear.None)
            {
                if (character._equippedItem != Gear.None)
                {
                    Game._monsterPartyGear.Add(character._equippedItem);
                }
                character._equippedItem = Game._monsterPartyGear[Game.random.Next(Game._monsterPartyGear.Count)]; // Equipt random piece of gear
                Game._monsterPartyGear.Remove(character._equippedItem);
                equippedItem = GetFriendlyItemName(character);
                AnsiConsole.Write(new Markup($"[steelblue1_1]{character._name}[/] equipped {equippedItem}!\n\n"));
                return "Used Item";
            }

            int choice = Game.random.Next(10);
            if (choice == 0) return "Do Nothing";
            else if (choice > 0 && character._equippedItem != Gear.None && character._equippedItem != Gear.BinaryHelm) return $"Gear Attack ({equippedItem})";
            else return "Standard Attack (UNRAVELING)";
        }
        else if (character._name == "SKELETON")
        {
            if (Game._monsterPartyInventory.Contains<Item>(Item.SimulasSoup) && character._health < character._maxHealth / 2 && Game.random.Next(4) == 0)
            {
                AnsiConsole.Write(new Markup($"[steelblue1_1]{character._name}[/] used [tan]Simula's Soup[/] on self!\n"));
                character._health = character._maxHealth;
                Game._monsterPartyInventory.Remove(Item.SimulasSoup);
                return "Used Item";
            }
            if (Game._monsterPartyInventory.Contains<Item>(Item.HealthPotion) && character._health < character._maxHealth / 2 && Game.random.Next(4) == 0)
            {
                character.Defend(-10, character, false);
                Game._monsterPartyInventory.Remove(Item.HealthPotion);
                AnsiConsole.Write(new Markup($"[steelblue1_1]{character._name}[/] used [red]Health Potion[/] on self!\n\n"));
                return "Used Item";
            }

            string equippedItem;
            equippedItem = GetFriendlyItemName(character);

            if (Game._monsterPartyGear.Count > 0 && Game.random.Next(2) == 1 && character._equippedItem == Gear.None)
            {
                if (character._equippedItem != Gear.None)
                {
                    Game._monsterPartyGear.Add(character._equippedItem);
                }
                character._equippedItem = Game._monsterPartyGear[Game.random.Next(Game._monsterPartyGear.Count)]; // Equipt random piece of gear
                Game._monsterPartyGear.Remove(character._equippedItem);
                equippedItem = GetFriendlyItemName(character);

                AnsiConsole.Write(new Markup($"[steelblue1_1]{character._name}[/] equipped {equippedItem}!\n\n"));
                return "Used Item";
            }

            int choice = Game.random.Next(10);
            if (choice == 0) return "Do Nothing";
            else if (choice > 0 && character._equippedItem != Gear.None && character._equippedItem != Gear.BinaryHelm) return $"Gear Attack ({equippedItem})";
            else return "Standard Attack (BONE CRUNCH)";
        }
        else if (character._name == "STONE AMAROK")
        {
            if (Game._monsterPartyInventory.Contains<Item>(Item.SimulasSoup) && character._health < character._maxHealth / 2 && Game.random.Next(4) == 0)
            {
                AnsiConsole.Write(new Markup($"[steelblue1_1]{character._name}[/] used [tan]Simula's Soup[/] on self!\n"));
                character._health = character._maxHealth;
                Game._monsterPartyInventory.Remove(Item.SimulasSoup);
                return "Used Item";
            }
            if (Game._monsterPartyInventory.Contains<Item>(Item.HealthPotion) && character._health < character._maxHealth / 2 && Game.random.Next(4) == 0)
            {
                character.Defend(-10, character, false);
                Game._monsterPartyInventory.Remove(Item.HealthPotion);
                AnsiConsole.Write(new Markup($"[steelblue1_1]{character._name}[/] used [red]Health Potion[/] on self!\n\n"));
                return "Used Item";
            }

            string equippedItem;
            equippedItem = GetFriendlyItemName(character);

            if (Game._monsterPartyGear.Count > 0 && Game.random.Next(2) == 1 && character._equippedItem == Gear.None)
            {
                if (character._equippedItem != Gear.None)
                {
                    Game._monsterPartyGear.Add(character._equippedItem);
                }
                character._equippedItem = Game._monsterPartyGear[Game.random.Next(Game._monsterPartyGear.Count)]; // Equipt random piece of gear
                Game._monsterPartyGear.Remove(character._equippedItem);
                equippedItem = GetFriendlyItemName(character);

                AnsiConsole.Write(new Markup($"[steelblue1_1]{character._name}[/] equipped {equippedItem}!\n\n"));
                return "Used Item";
            }

            int choice = Game.random.Next(10);
            if (choice == 0) return "Do Nothing";
            else if (choice > 0 && character._equippedItem != Gear.None && character._equippedItem != Gear.BinaryHelm) return $"Gear Attack ({equippedItem})";
            else return "Standard Attack (BITE)";
        }
        else if (character._name == "SHADOW OCTOPOID")
        {
            if (Game._monsterPartyInventory.Contains<Item>(Item.SimulasSoup) && character._health < character._maxHealth / 2 && Game.random.Next(4) == 0)
            {
                AnsiConsole.Write(new Markup($"[steelblue1_1]{character._name}[/] used [tan]Simula's Soup[/] on self!\n"));
                character._health = character._maxHealth;
                Game._monsterPartyInventory.Remove(Item.SimulasSoup);
                return "Used Item";
            }
            if (Game._monsterPartyInventory.Contains<Item>(Item.HealthPotion) && character._health < character._maxHealth / 2 && Game.random.Next(4) == 0)
            {
                character.Defend(-10, character, false);
                Game._monsterPartyInventory.Remove(Item.HealthPotion);
                AnsiConsole.Write(new Markup($"[steelblue1_1]{character._name}[/] used [red]Health Potion[/] on self!\n\n"));
                return "Used Item";
            }

            string equippedItem;
            equippedItem = GetFriendlyItemName(character);

            if (Game._monsterPartyGear.Count > 0 && Game.random.Next(2) == 1 && character._equippedItem == Gear.None)
            {
                if (character._equippedItem != Gear.None)
                {
                    Game._monsterPartyGear.Add(character._equippedItem);
                }
                character._equippedItem = Game._monsterPartyGear[Game.random.Next(Game._monsterPartyGear.Count)]; // Equipt random piece of gear
                Game._monsterPartyGear.Remove(character._equippedItem);
                equippedItem = GetFriendlyItemName(character);

                AnsiConsole.Write(new Markup($"[steelblue1_1]{character._name}[/] equipped {equippedItem}!\n\n"));
                return "Used Item";
            }

            int choice = Game.random.Next(10);
            if (choice == 0) return "Do Nothing";
            else if (choice > 0 && character._equippedItem != Gear.None && character._equippedItem != Gear.BinaryHelm) return $"Gear Attack ({equippedItem})";
            else return "Standard Attack (GRAPPLE)";
        }
        else
        {
            if (Game._heroPartyInventory.Contains<Item>(Item.SimulasSoup) && character._health < character._maxHealth / 2 && Game.random.Next(4) == 0)
            {
                AnsiConsole.Write(new Markup($"[steelblue1_1]{character._name}[/] used [tan]Simula's Soup[/] on self!\n"));
                character._health = character._maxHealth;
                Game._heroPartyInventory.Remove(Item.SimulasSoup);
                return "Used Item";
            }
            if (Game._heroPartyInventory.Contains<Item>(Item.HealthPotion) && character._health < character._maxHealth / 2 && Game.random.Next(4) == 0)
            {
                character.Defend(-10, character, true);
                Game._heroPartyInventory.Remove(Item.HealthPotion);
                AnsiConsole.Write(new Markup($"[steelblue1_1]{character._name}[/] used [red]Health Potion[/] on self!\n\n"));
                return "Used Item";
            }

            string equippedItem;
            equippedItem = GetFriendlyItemName(character);

            if (Game._heroPartyGear.Count > 0 && Game.random.Next(2) == 1 && character._equippedItem == Gear.None)
            {
                if (character._equippedItem != Gear.None)
                {
                    Game._heroPartyGear.Add(character._equippedItem);
                }
                character._equippedItem = Game._heroPartyGear[Game.random.Next(Game._heroPartyGear.Count)]; // Equipt random piece of gear
                Game._heroPartyGear.Remove(character._equippedItem);
                equippedItem = GetFriendlyItemName(character);

                AnsiConsole.Write(new Markup($"[steelblue1_1]{character._name}[/] equipped {equippedItem}!\n\n"));
                return "Used Item";
            }

            int choice = Game.random.Next(10);
            if (choice == 0) return "Do Nothing";
            else if (choice > 0 && character._equippedItem == Gear.CannonofConsolas) return "Gear Attack (Cannon of Consolas)"; // manual override since cannon of consolas has spaces
            else if (choice > 0 && character._equippedItem != Gear.None && character._equippedItem != Gear.BinaryHelm) return $"Gear Attack ({equippedItem})";
            else return "Standard Attack (PUNCH)";
        }
    }

    private static string GetFriendlyItemName(Character character)
    {
        string equippedItem;
        if (character._equippedItem == Gear.CannonofConsolas) equippedItem = "Cannon of Consolas";
        else if (character._equippedItem == Gear.Dagger) equippedItem = "Dagger";
        else if (character._equippedItem == Gear.Sword) equippedItem = "Sword";
        else if (character._equippedItem == Gear.Bow) equippedItem = "Vin's Bow";
        else if (character._equippedItem == Gear.BinaryHelm) equippedItem = "Binary Helm";
        else equippedItem = "None";
        return equippedItem;
    }

    public static int GetRandom(int max)
    {
        return Game.random.Next(max + 1);
    }
}