﻿
// Taking a Number

int AskForNumber(string text)
{
    Console.WriteLine(text);
    int response = Convert.ToInt32(Console.ReadLine());
    return response;
}

int AskForNumberInRange(string text, int min, int max)
{
    int response;
    do
    {
        Console.WriteLine(text);
        response = Convert.ToInt32(Console.ReadLine);

        if (response > min && response < max)
        {
            break;
        }

    } while (response < min || response > max);

    return response;
}

// Countdown

int num = 10;
Console.WriteLine("And finally... " + CountDown(num));
int CountDown(int number)
{
    if (number == 1)
    {
        return 1;
    }
    else
    {
        Console.WriteLine(number);
        return CountDown(number - 1);
    }
}