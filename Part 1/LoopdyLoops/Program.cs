﻿
// The Prototype

int num;
do
{
    Console.WriteLine("User 1, enter a number between 0 and 100: ");
    num = Convert.ToInt32(Console.ReadLine());
    Console.Clear();
}
while (num > 100 || num < 0);

int numGuess;

do
{
    Console.Write("User 2, guess the number: ");
    numGuess = Convert.ToInt32(Console.ReadLine());

    if (numGuess == num)
    {
        Console.WriteLine("You guessed the number!");
        break;
    }
    else if (numGuess > num)
    {
        Console.WriteLine("Your guess was too high.");
    }
    else if (numGuess < num)
    {
        Console.WriteLine("Your guess was too low.");
    }
    else
    {
        Console.WriteLine("Your guess was invalid.");
    }
} while (true);

Console.WriteLine("Press any key to activate the magic cannon.");

// The Magic Cannon

Console.ReadKey(true);
Console.Clear();

for (int i = 1; i <= 100; i++)
{
    if (i % 3 == 0 && i % 5 == 0)
    {
        Console.WriteLine(i + ": Combined");
    }
    else if (i % 3 == 0)
    {
        Console.WriteLine(i + ": Fire");
    }
    else if (i % 5 == 0)
    {
        Console.WriteLine(i + ": Electric");
    }
    else
    {
        Console.WriteLine(i + ": Normal");
    }
}