﻿
bool var14 = false;
decimal var13 = 3.5623m;
double var12 = 3.5623f;
float var11 = 0.0f;
string var10 = "Hello, galaxy!";
char var9 = 'A';
int var8 = 0;
ulong var7 = 10_000_000_000_000_000UL;
uint var6 = 4_294_967_295;
ushort var5 = 65_535;
sbyte var4 = 127;
long var3 = 9_223_372_036_854_775_807;
short var2 = 32_767;
byte var1 = 255;

Console.WriteLine("Byte: " + var1);
Console.WriteLine("Short: " + var2);
Console.WriteLine("Long: " + var3);
Console.WriteLine("SByte: " + var4);
Console.WriteLine("UShort: " + var5);
Console.WriteLine("UInt: " + var6);
Console.WriteLine("ULong: " + var7);
Console.WriteLine("Int: " + var8);
Console.WriteLine("Char: " + var9);
Console.WriteLine("String: " + var10);
Console.WriteLine("Float: " + var11);
Console.WriteLine("Double: " + var12);
Console.WriteLine("Decimal: " + var13);
Console.WriteLine("Bool: " + var14);

Console.WriteLine("Now changing variables.");

var14 = true;
var13 = 3.14159m;
var12 = 3.14159f;
var11 = 3.14159f;
var10 = "Hello, world!";
var9 = 'B';
var8 = 1;
var7 = 100_000_000_000;
var6 = 1_000_000_000;
var5 = 10_000;
var4 = -126;
var3 = 1_000_000_000_000_000_000;
var2 = 10_000;
var1 = 100;

Console.WriteLine("Byte: " + var1);
Console.WriteLine("Short: " + var2);
Console.WriteLine("Long: " + var3);
Console.WriteLine("SByte: " + var4);
Console.WriteLine("UShort: " + var5);
Console.WriteLine("UInt: " + var6);
Console.WriteLine("ULong: " + var7);
Console.WriteLine("Int: " + var8);
Console.WriteLine("Char: " + var9);
Console.WriteLine("String: " + var10);
Console.WriteLine("Float: " + var11);
Console.WriteLine("Double: " + var12);
Console.WriteLine("Decimal: " + var13);
Console.WriteLine("Bool: " + var14);
