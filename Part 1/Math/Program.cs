﻿
// The Triangle Farmer
Console.WriteLine("Input the base measurement of the triangle: ");
float baseOfTriangle = Convert.ToSingle(Console.ReadLine());
Console.WriteLine("Input the height measurement of the triangle: ");
float heightOfTriangle = Convert.ToSingle(Console.ReadLine());

float areaOfTriangle = (baseOfTriangle * heightOfTriangle) / 2;

Console.WriteLine("Area of triangle: " + areaOfTriangle);

// The Four Sisters and the Duckbear
Console.ReadLine();
Console.Clear();
Console.WriteLine("How many eggs did you collect today?: ");
int eggsCollected = Convert.ToInt32(Console.ReadLine());
int duckBearEggs = eggsCollected % 4;
int sisterEggs = (eggsCollected - duckBearEggs) / 4;

Console.WriteLine("Feed the duckbear " + duckBearEggs + " egg(s). Give the sisters " + sisterEggs + " egg(s) each.");

// The Dominion of Kings
Console.ReadLine();
Console.Clear();
Console.WriteLine("Enter the number of estates your kindom possesses: ");
int estatePoints =  Convert.ToInt32(Console.ReadLine()) * 1;
Console.WriteLine("Enter the number of duchys your kindom possesses: ");
int duchyPoints = Convert.ToInt32(Console.ReadLine()) * 3;
Console.WriteLine("Enter the number of provinces your kindom possesses: ");
int provincePoints = Convert.ToInt32(Console.ReadLine()) * 6;
Console.WriteLine("Your kinddom is worth " + (estatePoints + duchyPoints + provincePoints) + " points.");