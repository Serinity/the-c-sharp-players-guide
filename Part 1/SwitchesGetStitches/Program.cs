﻿
// Buying Inventory and Discounted Inventory

Console.Write("What is your name?: ");
string name = Console.ReadLine();
Console.Clear();

Console.WriteLine("The following items are available: \n" +
    "1 - Rope \n" +
    "2 - Torches \n" +
    "3 - Climbing Equipment \n" +
    "4 - Clean Water \n" +
    "5 - Machete \n" +
    "6 - Canoe \n" +
    "7 - Food Supplies");

Console.Write("What number do you want to see the price of?: ");
int itemSelected = Convert.ToInt32(Console.ReadLine());

double itemPrice;

itemPrice = itemSelected switch
{
    1 => 10,
    2 => 15,
    3 => 25,
    4 => 1,
    5 => 20,
    6 => 200,
    7 => 1,
    _ => 0
};

switch(name)
{
    case "Serinity":
        itemPrice = itemPrice * .5;
        break;
    default:
        break;
}

Console.WriteLine($"Item {itemSelected} will cost you {itemPrice} dubloon(s), {name}.");