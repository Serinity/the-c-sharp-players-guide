﻿
//The Defense of Consolas
Console.Title = "Defense of Consolas";

Console.Write("Target Row? ");
int targetRow = Convert.ToInt32(Console.ReadLine());
Console.Write("Target Column? ");
int targetCol = Convert.ToInt32(Console.ReadLine());

Console.Clear();
Console.BackgroundColor = ConsoleColor.Red;
Console.ForegroundColor = ConsoleColor.Black;

Console.WriteLine("Deploy to:");
Console.WriteLine($"({targetRow}, {targetCol - 1})");
Console.WriteLine($"({targetRow - 1}, {targetCol})");
Console.WriteLine($"({targetRow}, {targetCol + 1})");
Console.WriteLine($"({targetRow + 1}, {targetCol})");

Console.Beep();