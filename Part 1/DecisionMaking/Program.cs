﻿
// Repairing the Clocktower

Console.Write("Please enter a number: ");

int number = Convert.ToInt32(Console.ReadLine());

int numberRemainder = number % 2;

if (numberRemainder == 0)
{
    Console.WriteLine("Tick");
}
else
{
    Console.WriteLine("Tock");
}

// Watchtower
Console.WriteLine("Press any key to continue...");
Console.ReadKey(true);
Console.Clear();

Console.Write("Please enter X coordinate: ");
int x  = Convert.ToInt32(Console.ReadLine());
Console.Write("Please enter Y coordinate: ");
int y = Convert.ToInt32(Console.ReadLine());

if (x == 0 && y == 0)
{
    Console.Clear();
    Console.BackgroundColor = ConsoleColor.Red;
    Console.WriteLine("!");
}
else if (x < 0 && y > 0)
{
    Console.WriteLine("NW");
}
else if (x < 0 && y == 0)
{
    Console.WriteLine("W");
}
else if (x < 0 && y < 0)
{
    Console.WriteLine("SW");
}
else if (x == 0 && y > 0)
{
    Console.WriteLine("N");
}
else if (x == 0 && y < 0)
{
    Console.WriteLine("S");
}
else if (x > 0 && y > 0)
{
    Console.WriteLine("NE");
}
else if (x > 0 && y == 0)
{
    Console.WriteLine("E");
}
else if (x > 0 && y < 0 )
{
    Console.WriteLine("SE");
}
else
{
    Console.WriteLine("Invalid Direction.");
}