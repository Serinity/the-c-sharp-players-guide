﻿
// Hunting the Manticore

//Step 1: Establish the game's starting state
int manticoreHealth = 10;
int cityHealth = 15;
int manticoreDistance;
int round = 1;
int damage;

Random random = new Random();
manticoreDistance = random.Next(0, 101);

//Old step 2, before randomness was added to make game singleplayer.
//Step 2: Ask player 1 to choose Manticore's distance from the city. Clear screen afterward.
//do
//{
//    Console.Write("Player 1. Please enter from 0 to 100 how far away the Manticore is: ");
//    manticoreDistance = Convert.ToInt32(Console.ReadLine());
//} while (manticoreDistance < 0 || manticoreDistance > 100);
//Console.Clear();

//Step 3: Run the game in a loop until either the manticore's or city's health reaches 0
Console.WriteLine("Player 2, it is your turn.");
do
{
    Console.WriteLine("----------------------------------------------------------------------------");
    Console.WriteLine($"STATUS: Round {round}. City: {cityHealth}. Manticore: {manticoreHealth}");

    damage = MagicCannon(round);
    Console.WriteLine($"The cannon is expected to deal {damage} damage this round.");

    //Step 5: Get target range from second player, resolve it's effect.
    Console.Write("Enter the desired cannon range: ");
    int cannonRange = Convert.ToInt32(Console.ReadLine());
    Console.WriteLine(FireCannon(cannonRange));
    //Reset console colors back to default
    Console.BackgroundColor = ConsoleColor.Black;
    Console.ForegroundColor = ConsoleColor.White;

    //Step 6: Reduce city health if manticore is still alive.
    if (manticoreHealth > 0)
    {
        --cityHealth;
    }

    //Step 7: Advance to next round...
    round++;
} while (manticoreHealth > 0 && cityHealth > 0);

//Step 8: Display end game
EndGame();

//Step 4: Compute how much damage the cannon will deal this round.
int MagicCannon(int r)
{
    if (r % 3 == 0 && r % 5 == 0)
    {
        return 10;
    }
    else if (r % 3 == 0 || r % 5 == 0)
    {
        return 3;
    }
    else
    {
        return 1;
    }
}

string FireCannon(int range)
{
    if (range == manticoreDistance)
    {
        manticoreHealth -= damage;
        //Step 9: Change colors for different types of messages.
        Success();
        return "That round was a DIRECT HIT!";
    }
    else if (range > manticoreDistance)
    {
        Fail();
        return "That round OVERSHOT the target.";
    }
    else
    {
        Fail();
        return "That round FELL SHORT of the target.";
    }
}

void EndGame()
{
    if (manticoreHealth <= 0)
    {
        Success();
        Console.WriteLine("The Manticore has been destroyed. The city of Consolas has been saved!");
    }
    else if (cityHealth <= 0)
    {
        Fail();
        Console.WriteLine("The Manticore has succeeded. The city of Consolas has been destroyed.");
    }
    else
    {
        Console.BackgroundColor = ConsoleColor.Yellow;
        Console.ForegroundColor = ConsoleColor.Black;
        Console.WriteLine("Nobody wins...?"); // This should never happen.
    }
    Console.BackgroundColor = ConsoleColor.Black;
    Console.ForegroundColor = ConsoleColor.White;
            
}

void Success()
{
    Console.BackgroundColor = ConsoleColor.Green;
    Console.ForegroundColor = ConsoleColor.Black;
}

void Fail()
{
    Console.BackgroundColor = ConsoleColor.Red;
    Console.ForegroundColor = ConsoleColor.Black;
}