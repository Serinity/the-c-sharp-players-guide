﻿
// The Replicator of D'To

int[] original = new int[5];

original[0] = AskForNumber("Please enter first number: ");
original[1] = AskForNumber("Please enter second number: ");
original[2] = AskForNumber("Please enter third number: ");
original[3] = AskForNumber("Please enter fourth number: ");
original[4] = AskForNumber("Please enter fifth number: ");

Console.WriteLine("Copying original array to copy array...");

int[] copy = new int[5];

Console.WriteLine("Original array:");
for(int index = 0; index < original.Length; index++)
{
    copy[index] = original[index];
    Console.WriteLine(original[index]);
}


Console.WriteLine("Copy array:");
for(int index = 0; index < copy.Length; index++)
{
    Console.WriteLine(copy[index]);
}

// The Laws of Freach

int[] array = new int[] { 4, 51, -7, 13, -99, 15, -8, 45, 90 };

int total = 0;
foreach(int nums in array)
{
    total += nums;
}

float average = (float)total / array.Length;
Console.WriteLine("Average: " + average);

//Taking a Number

int guess = AskForNumberInRange("Please guess a number between 1 and 10", 1, 10);
Console.WriteLine($"You guessed {guess}");

int AskForNumber(string text)
{
    Console.WriteLine(text);
    int response = Convert.ToInt32(Console.ReadLine());
    return response;
}

int AskForNumberInRange(string text, int min, int max)
{
    int response;
    do
    {
        Console.WriteLine(text);
        response = Convert.ToInt32(Console.ReadLine());

        if (response > min && response < max)
        {
            break;
        }

    } while (response < min || response > max);

    return response;
}