﻿
// The Repeating Stream

int randomNum = -1;
RecentNumbers recentNumbers = new RecentNumbers();

Thread thread = new Thread(RunNums);

thread.Start();

while (true)
{
    Console.ReadKey(true);
    if (recentNumbers.MostRecentNum == recentNumbers._2ndMostRecentNum) Console.WriteLine("You successfully identified two identical numbers.");
    else Console.WriteLine("Those numbers are not the same.");
}


void RunNums()
{
    while (true)
    {
        if (randomNum != -1) recentNumbers.Set2ndMostRecentNum(randomNum);
        Random random = new Random();
        randomNum = random.Next(0, 10);
        Console.WriteLine(randomNum);
        recentNumbers.SetMostRecentNum(randomNum);
        Thread.Sleep(1000);
    }
}



class RecentNumbers
{
    private readonly object _mostRecentLock = new object();
    private readonly object _2ndMostRecentLock = new object();
    private int _mostRecentNum;
    private int _2ndmostRecentNum;

    public void SetMostRecentNum(int num)
    {
        _mostRecentNum = num;
    }

    public void Set2ndMostRecentNum(int num)
    {
        _2ndmostRecentNum = num;
    }

    public int MostRecentNum
    {
        get
        {
            lock (_mostRecentLock)
            {
                return _mostRecentNum;
            }
        }
    }

    public int _2ndMostRecentNum
    {
        get
        {
            lock (_2ndMostRecentLock)
            {
                return _2ndmostRecentNum;
            }
        }
    }
}