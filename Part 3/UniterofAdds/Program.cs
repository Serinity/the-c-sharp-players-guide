﻿
// Uniter of Adds

Console.WriteLine(Adds.Add(1, 1));
Console.WriteLine(Adds.Add(1.1, 1.2));
Console.WriteLine(Adds.Add("one, ", "two"));
DateTime dateTime = DateTime.Now;
TimeSpan timeSpan = TimeSpan.FromSeconds(100000);
Console.WriteLine(Adds.Add(dateTime, timeSpan));

public static class Adds
{
    public static dynamic Add(dynamic a, dynamic b) => a + b;
}