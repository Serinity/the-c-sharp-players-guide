﻿
// The Three Lenses

int[] numArray = new int[9] { 1, 9, 2, 8, 3, 7, 4, 6, 5 };

List<int> numList = MakeAList(numArray);

PrintNums(numList);

numList = CheckItTwice(numArray);

PrintNums(numList);

numList = GonnaFindOutWhosNaughtyOrNice(numArray);

PrintNums(numList);

List<int> MakeAList(int[] ints)
{
    Array.Sort(ints); // Sorts ints in ascending order

    List<int> withoutOdds = new List<int>();

    for (int i = 0; i < ints.Length; i++)
    {
        if (ints[i] % 2 == 0) withoutOdds.Add(ints[i]); // Negate odd numbers
    }

    for (int i = 0; i < withoutOdds.Count; i++)
    {
        withoutOdds[i] = withoutOdds[i] * 2; // Double nums
    }

    return withoutOdds;
}

List<int> CheckItTwice(int[] ints)
{
    IEnumerable<int> completedList = from i in ints
                                     orderby i
                                     where i % 2 == 0
                                     select i * 2;
    return completedList.ToList<int>();
}

List<int> GonnaFindOutWhosNaughtyOrNice(int[] ints)
{
    IEnumerable<int> completedList = ints
        .Where(i => i % 2 == 0)
        .OrderBy(i => i)
        .Select(i => i * 2);

    return completedList.ToList<int>();
}

void PrintNums(List<int> list)
{
    foreach (int num in list)
    {
        Console.Write($"{num} ");
    }
    Console.WriteLine();
}