﻿
// Exepti's Game

Random random = new Random();

int oatmealRaisinCookie = random.Next(0, 10); // 0-9

List<int> list = new List<int>(); // Stores player's guesses

int Player = 1;

while (true) // Gameplay loop
{
    Console.WriteLine($"Player {Player}, pick a number between 0 and 9 (inclusive) that has not yet been picked.");
    int input = Convert.ToInt32(Console.ReadLine());

    try
    {
        if (list.Contains(input))
        {
            Console.WriteLine("That number has already been picked.");
            continue;
        }
        else if (input == oatmealRaisinCookie) throw new OatmealCookieException();
        else list.Add(input);
    }
    catch (OatmealCookieException)
    {
        Console.WriteLine($"You picked the oatmeal raisin cookie! Some say you are the loser, but in my heart Player {Player} is the winner!");
        break;
    }
    

    if (Player == 1) Player++; // Alternates between players 1 and 2.
    else Player--;
}

public class OatmealCookieException : Exception // Custom exception for when user picks an oatmeal raisin cookie (the right cookie imo)
{
    public OatmealCookieException() { }
    public OatmealCookieException(string message) : base(message) { }
}