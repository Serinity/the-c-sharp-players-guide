﻿
// The Sieve && The Lambda Sieve

Console.WriteLine("Which numbers are good to you?\n1. Even Numbers\n2. Positive Numbers\n3. Multiples of Ten");
int input = Convert.ToInt32(Console.ReadLine());

Sieve sieve;

sieve = input switch
{
    1 => new Sieve(n => n % 2 == 0),
    2 => new Sieve(n => n > 0),
    3 => new Sieve(n => n % 10 == 0)
};

Console.Clear();

Console.WriteLine("Input numbers for as long as your heart desires to see if they are good: ");
while (true)
{
    int num = Convert.ToInt32(Console.ReadLine());
    Console.WriteLine(sieve.IsGood(num));
}

public class Sieve
{
    private Func<int, bool> _function;

    public Sieve(Func<int, bool> function)
    {
        _function = function;
    }

    public bool IsGood (int number)
    {
        return _function(number);
    }
}