﻿
// Safer Number Crunching

int number;
do
{
    Console.WriteLine("Please enter an int value");
} while (!int.TryParse(Console.ReadLine(), out number));

Console.WriteLine(number);

double number2;
do
{
    Console.WriteLine("Please enter a valid number");
} while (!double.TryParse(Console.ReadLine(), out number2));

Console.WriteLine(number2);

bool trueOrFalse;
do
{
    Console.WriteLine("Please enter true or false");
} while (!bool.TryParse(Console.ReadLine(), out trueOrFalse));

Console.WriteLine(trueOrFalse);