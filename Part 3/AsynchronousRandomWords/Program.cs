﻿
// Asynchronous Random Words and Many Random Words, done solo


while (true)
{
    Console.WriteLine("Please enter a word: ");
    string input = Console.ReadLine();
    Results(input);
}

int RandomlyRecreate(string word)
{
    Random random = new Random();

    string wordGuess = "";
    int attempts = 0;

    do
    {
        wordGuess = "";
        for (int i = 0; i < word.Length; i++)
        {
            wordGuess += (char)('a' + random.Next(26));
        }
        attempts++;
    } while (wordGuess != word);

    return attempts;
}

Task<int> RandomlyRecreateAsync(string word)
{
    return Task.Run(() => RandomlyRecreate(word));
}


async void Results(string word)
{
    DateTime start = DateTime.Now;
    int attempts = await RandomlyRecreateAsync(word);
    Console.WriteLine($"{word}:");
    Console.WriteLine(attempts);
    Console.WriteLine(DateTime.Now - start);
}