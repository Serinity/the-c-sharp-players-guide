﻿
// The Robot Factory

using System.Dynamic;

dynamic robot = new ExpandoObject();

int robotID = 0;
while (true)
{
    robot.ID = robotID++;
    Console.WriteLine($"You are producing robot #{robot.ID}");
    Console.Write("Do you want to name this robot? ");
    string input = Console.ReadLine();
    if (input == "yes")
    {
        Console.Write("What is it's name? ");
        robot.Name = Console.ReadLine();
    }
    Console.Write("Does this robot have a specific size? ");
    input = Console.ReadLine();
    if (input == "yes")
    {
        Console.Write("What is it's height? ");
        robot.Height = Console.ReadLine();
        Console.Write("What is it's width? ");
        robot.Width = Console.ReadLine();
    }
    Console.Write("Does this robot need to be a specific color? ");
    input = Console.ReadLine();
    if (input == "yes")
    {
        Console.Write("What color? ");
        robot.Color = Console.ReadLine();
    }

    foreach (KeyValuePair<string, object> property in (IDictionary<string, object>)robot)
        Console.WriteLine($"{property.Key}: {property.Value}");

}
