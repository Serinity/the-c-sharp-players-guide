﻿
// Better Random

Random random = new Random();

Console.WriteLine(random.RandomDouble(100));
Console.WriteLine(random.NextString("Red", "Green", "Blue"));
Console.WriteLine(random.CoinFlip());
Console.WriteLine(random.CoinFlip(.25));

public static class RandomExtensions
{
    public static double RandomDouble (this Random random, double max)
    {
        double output = random.NextDouble() * max;
        return output;
    }

    public static string NextString (this Random random, params string[] strings)
    {
        int index = random.Next(strings.Length);
        return strings[index];
    }

    public static bool CoinFlip (this Random random, double chanceOfHeads = .5)
    {
        return random.NextDouble() < chanceOfHeads;
    }
}