﻿
// The Great Humanizer

using Humanizer;

Console.WriteLine($"When is the feast? {DateTime.UtcNow.AddHours(30)}");

Console.WriteLine($"When is the feast? {DateTime.UtcNow.AddHours(30).Humanize()}");

Console.WriteLine($"When is the feast? {DateTime.UtcNow.AddHours(24).Humanize()}");

Console.WriteLine($"When is the feast? {DateTime.UtcNow.AddHours(72).Humanize()}");