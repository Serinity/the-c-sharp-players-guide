﻿
// The Long Game

Console.Write("Please enter your name: ");
string? username = Console.ReadLine();

int playerScore = 0;

if (File.Exists($"{username}.txt"))
{
    playerScore = Convert.ToInt32(File.ReadAllText($"{username}.txt"));
}

while (true)
{
    Console.Clear();
    Console.Write("Player score: " + playerScore);
    if (Console.ReadKey().Key == ConsoleKey.Enter) break;
    else playerScore++;
}

File.WriteAllText($"{username}.txt", playerScore.ToString());