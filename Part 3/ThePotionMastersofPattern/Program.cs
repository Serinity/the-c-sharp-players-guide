﻿
// The Potion Masters of Pattern
Potion potion = new Potion();

while (true)
{
    Console.Clear();
    Console.WriteLine($"You currently have a {potion.Type} potion.");
    if (potion.Type == PotionType.Ruined)
    {
        Console.WriteLine("You'll need to start over with water. Press any key to continue.");
        Console.ReadKey(true);
        potion.Type = PotionType.Water;
        continue;
    }
    Console.WriteLine("What ingredient would you like to add? Star Dust, Snake Venom, Dragon Breath, Shadow Glass, Eyeshine Gem");

    string? input = Console.ReadLine().ToLower();

    Ingredient ingredient = input switch
    {
        "star dust" => Ingredient.StarDust,
        "snake venom" => Ingredient.SnakeVenom,
        "dragon breath" => Ingredient.DragonBreath,
        "shadow glass" => Ingredient.ShadowGlass,
        "eyeshine gem" => Ingredient.EyeshineGem
    };

    potion.Type = (potion.Type, ingredient) switch
    {
        (PotionType.Water, Ingredient.StarDust) => PotionType.Elixer,
        (PotionType.Elixer, Ingredient.SnakeVenom) => PotionType.Poision,
        (PotionType.Elixer, Ingredient.DragonBreath) => PotionType.Flying,
        (PotionType.Elixer, Ingredient.ShadowGlass) => PotionType.Invisibility,
        (PotionType.Elixer, Ingredient.EyeshineGem) => PotionType.NightSight,
        (PotionType.NightSight, Ingredient.ShadowGlass) => PotionType.Cloudy,
        (PotionType.Invisibility, Ingredient.EyeshineGem) => PotionType.Cloudy,
        (PotionType.Cloudy, Ingredient.StarDust) => PotionType.Wraith,
        (_, _) => PotionType.Ruined
    };

    Console.Clear();
    Console.WriteLine($"You have created a {potion.Type} potion.");
    Console.WriteLine("Would you like to continue, or complete your potion?");
    input = Console.ReadLine().ToLower();

    if (input == "complete") break;
}

public class Potion
{
    public PotionType Type { get; set; }
}

public enum PotionType { Water, Elixer, Poision, Flying, Invisibility, NightSight, Cloudy, Wraith, Ruined }
public enum Ingredient { StarDust, SnakeVenom, DragonBreath, ShadowGlass, EyeshineGem }