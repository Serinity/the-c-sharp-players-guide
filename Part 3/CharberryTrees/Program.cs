﻿
// Charberry Trees

CharberryTree tree = new CharberryTree();
Notifier notifier = new Notifier(tree);
Harvester harvester = new Harvester(tree);

while (true) tree.MaybeGrow();

public class CharberryTree
{
    private Random _random = new Random();
    public bool Ripe { get; set; }
    public event Action<CharberryTree>? Ripened;

    public void MaybeGrow()
    {
        // Only a tiny chance of ripening each time, but we try a lot!
        if (_random.NextDouble() < 0.00000001 && !Ripe)
        {
            Ripe = true;
            Ripened(this);
        }
    }
}

public class Notifier
{
    private void OnTreeRipened(CharberryTree tree) => Console.WriteLine("Tree has ripened!");
    public Notifier(CharberryTree tree)
    {
        tree.Ripened += OnTreeRipened;
    }
}

public class Harvester
{
    private void OnTreeRipe(CharberryTree tree)
    {
        tree.Ripe = false;
        Console.WriteLine("Fruit picked!");
    }
        

    public Harvester(CharberryTree tree)
    {
        tree.Ripened += OnTreeRipe;
    }
}