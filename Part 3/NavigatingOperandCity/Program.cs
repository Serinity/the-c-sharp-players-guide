﻿
// Navigating Operand City, Indexing Operand City, and Converting Directions to Offsets

Console.WriteLine(new BlockCoordinate(1, 1) + new BlockOffset(2, 2));
Console.WriteLine(new BlockCoordinate(0, 0) + Direction.South);
BlockCoordinate blockCoordinate = new BlockCoordinate(0, 1);
Console.WriteLine(blockCoordinate[0]);
Console.WriteLine(blockCoordinate[1]);
BlockOffset blockOffset = Direction.North;
Console.WriteLine(blockOffset);

public record BlockCoordinate(int Row, int Column)
{
    public static BlockCoordinate operator +(BlockCoordinate a, BlockOffset b) =>
        new BlockCoordinate(a.Row + b.RowOffset, a.Column + b.ColumnOffset);
    public static BlockCoordinate operator +(BlockOffset a, BlockCoordinate b) =>
        b + a;

    public static BlockCoordinate operator +(BlockCoordinate a, Direction b)
    {
        return a + (b switch
        {
            Direction.North => new BlockOffset(-1, 0),
            Direction.South => new BlockOffset(1, 0),
            Direction.East => new BlockOffset(0, 1),
            Direction.West => new BlockOffset(0, -1)
        });
    }

    public double this[int index]
    {
        get
        {
            if (index == 0) return Row;
            else return Column;
        }
    }
};
public record BlockOffset(int RowOffset, int ColumnOffset)
{
    public static implicit operator BlockOffset(Direction direction)
    {
        switch(direction)
        {
            case Direction.North:
                return new BlockOffset(-1, 0);
            case Direction.South:
                return new BlockOffset(1, 0);
            case Direction.East:
                return new BlockOffset(0, 1);
            case Direction.West:
                return new BlockOffset(0, -1);
            default:
                return new BlockOffset(0, 0);
        }
    }
};
public enum Direction { North, East, South, West }