Hello! If you've run across this repository, you've found my adventure in learning C# using 
The C# Player's Guide. This repository contains all practices, projects, and solutions that 
I've created throughout the course of this book. Thanks for stopping by!